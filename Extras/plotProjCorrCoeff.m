%%function plotProjCorrCoeff
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Plot the monthly averages used to calculate the anomalies
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


corrCoeffNorth = [...
   [0.026, 0.412, 0.549, 0.582, 0.562, 0.712];... (* Model on data, North *)
   [-0.019, 0.198, 0.408, 0.556, 0.653, 0.849]... (* Data on model, North *)
];
globErrorsNorth = [...
    [15.8235    6.0297    4.3479    3.4855    3.1736    2.6113];... (* Model on data *)
    [18.6520    8.5477    6.2768    4.5038    3.3455    2.4959]...  (* Data on model *)
];
% corrCoeff = [...
%    [0.023, 0.416, 0.541, 0.588, 0.573, 0.731];... (* Model on data, North *)
%    [-0.018, 0.185, 0.374, 0.498, 0.540, 0.725]... (* Data on model, North *)
% ];
corrCoeffSouth = [...
   [0.337, 0.575, 0.605, 0.640, 0.669, 0.695];... (* Model on data, South *)
   [-0.003, 0.044, 0.137, 0.518, 0.657, 0.684]... (* Data on model, South *)
];
globErrorsSouth = [...
    [7.2101    3.4475    3.1312    2.7465    2.4248    2.2931];... (* Model on data *)
    [11.2924    4.9428    4.1555    2.9577    2.4112    2.2773]... (* Data on model *)
];
% corrCoeffSouth = [...
%    [0.415, 0.496, 0.567, 0.596, 0.618, 0.660];... (* Model on data, South *)
%    [-0.008, 0.115, 0.154, 0.483, 0.591, 0.625]... (* Data on model, South *)
% ];




corrCoeffAssim = [...
   [0.269, 0.667, 0.751, 0.775, 0.799, 0.856];... (* Model on data, North *)
   [0.241, 0.255, 0.538, 0.676, 0.751, 0.870]... (* Data on model, North *)
];

corrCoeffAssimSigmas = flip([2 1 0.5 0.25 0.1 0.05 0.01]);
%corrCoeffAssim0yearAvg  = flip([0.2823    0.2824    0.2823    0.2814    0.2738    0.2532    0.0934]);
corrCoeffAssim1yearAvg  = flip([0.6748    0.6771    0.6825    0.6889    0.6782    0.6344    0.4652]);
corrCoeffAssim2yearAvg  = flip([0.7440    0.7468    0.7534    0.7620    0.7583    0.7286    0.5960]);
corrCoeffAssim5yearAvg  = flip([0.7519    0.7551    0.7628    0.7761    0.7810    0.7536    0.6292]);
corrCoeffAssim10yearAvg = flip([0.7905    0.7927    0.7993    0.8117    0.8093    0.7693    0.6183]);
corrCoeffAssim20yearAvg = flip([0.8547    0.8563    0.8602    0.8664    0.8612    0.8397    0.7496]);

%globalErrorsAssim0yearAvg = flip([4.2191    4.2050    4.1541    3.9933    3.3894    2.5849    1.5424]);
globalErrorsAssim1yearAvg = flip([4.1242    4.1114    4.0814    4.0507    4.1456    4.4555    5.6390]);
globalErrorsAssim2yearAvg = flip([3.1970    3.1819    3.1455    3.0977    3.1254    3.3092    4.0804]);
globalErrorsAssim5yearAvg = flip([2.7850    2.7711    2.7358    2.6714    2.6326    2.7474    3.2846]);
globalErrorsAssim10yearAvg = flip([2.5037    2.4933    2.4659    2.4214    2.4233    2.5534    3.0114]);
globalErrorsAssim20yearAvg = flip([2.2393    2.2242    2.1944    2.1624    2.1677    2.2392    2.5123]);


corrCoeffDAEOF = [...
    [0.344 0.448 0.622 0.755 0.927];...  (* Using assimilated EOF *)
    [0.340 0.464 0.674 0.767 0.914]... (* Original, non-assimilated model *)
];
rmsDiffDAEOF = [...
    [5.802 4.931 4.485 4.592 4.886];...  (* Using assimilated EOF *)
    [6.553 5.250 4.381 4.455 4.717]... (* Original, non-assimilated model *)
];


% PC 2
corrCoeffPC2North = [...
    [0.037, -0.072, -0.147, 0.217, 0.427, 0.496];... (* Model on data, North *)
    [-0.001, 0.125, 0.218, 0.278, 0.341, 0.574];... (* Data on model, North *)
];
corrCoeffPC2South = [...
    [-0.040, 0.018, -0.068, -0.038, -0.290, -0.212];... (* Model on data, South *)
    [0.079, 0.061, 0.455, 0.344, -0.279, -0.129];... (* Data on model, South *)
];

% corrCoeffPC2 = [...
%     [0.037, -0.106, -0.187, 0.205, 0.377, 0.520];... (* North, Model on data *)
%     [-0.033, 0.053, -0.064, 0.009, -0.556, -0.509]... (* South, Model on data *)
% ];

corrCoeffPC2 = zeros(size(corrCoeffPC2North));
corrCoeffPC2(1,:) = corrCoeffPC2North(1,:);
corrCoeffPC2(2,:) = corrCoeffPC2South(2,:);
   
figure(1);
figuresize(380, 190, 'points');
clf; hold on;
plot(corrCoeffNorth(1,:), '.-', 'Color', [0.560181, 0.691569, 0.194885], 'MarkerSize', 20);
plot(corrCoeffNorth(2,:), '.-', 'Color', [0.880722, 0.611041, 0.14205], 'MarkerSize', 20);
xlim([0.8, 6.2]);
ylim([-0.1, 1]);
ax = gca;
ax.XTick = 1:6;
ax.XTickLabel = {'0','1','2','5','10','20'};
ax.YTick = 0:.2:1;
xlabel('Number of years average');
ylabel('Corr. coeff.');
ax.YGrid = 'on';
ax.FontSize = 12;
legend({'Model on data', 'Data on model'}, 'Location', 'best');

figure(2);
figuresize(380, 190, 'points');
clf; hold on;
plot(corrCoeffSouth(1,:), '.-', 'Color', [0.560181, 0.691569, 0.194885], 'MarkerSize', 20);
plot(corrCoeffSouth(2,:), '.-', 'Color', [0.880722, 0.611041, 0.14205], 'MarkerSize', 20);
xlim([0.8, 6.2]);
ylim([-0.1, 1]);
ax = gca;
ax.XTick = 1:6;
ax.XTickLabel = {'0','1','2','5','10','20'};
ax.YTick = 0:.2:1;
xlabel('Number of years average');
ylabel('Corr. coeff.');
ax.YGrid = 'on';
ax.FontSize = 12;
legend({'Model on data', 'Data on model'}, 'Location', 'best');

%% DAEOF

figure(95);
figuresize(380, 220, 'points');
b = bar(corrCoeffDAEOF','DisplayName','corrCoeffDAEOF');
b(2).FaceColor = [0.8 0.8 0.8];
b(1).FaceColor = [0.368417, 0.506779, 0.709798];
% clf; hold on;
% plot(corrCoeffDAEOF(1,:), '.-', 'Color', [0.560181, 0.691569, 0.194885], 'MarkerSize', 20);
% plot(corrCoeffDAEOF(2,:), '.-', 'Color', [0.880722, 0.611041, 0.14205], 'MarkerSize', 20);
% xlim([0.8, 6.2]);
ylim([0.3, 0.95]);
ax = gca;
colormap('lines');
% ax.XTick = 1:6;
ax.XTickLabel = {'1','2','5','10','20'};
% ax.YTick = 0:.2:1;
xlabel('Number of years average');
ylabel('Corr. coeff.');
ax.YGrid = 'on';
ax.FontSize = 12;
legend({'Assimilated EOF','Original model'}, 'Location', 'northwest');

figure(96);
figuresize(380, 220, 'points');
b = bar(rmsDiffDAEOF','DisplayName','corrCoeffDAEOF');
b(2).FaceColor = [0.8 0.8 0.8];
b(1).FaceColor = [0.368417, 0.506779, 0.709798];
% clf; hold on;
% plot(corrCoeffDAEOF(1,:), '.-', 'Color', [0.560181, 0.691569, 0.194885], 'MarkerSize', 20);
% plot(corrCoeffDAEOF(2,:), '.-', 'Color', [0.880722, 0.611041, 0.14205], 'MarkerSize', 20);
% xlim([0.8, 6.2]);
%ylim([0.3, 0.95]);
ax = gca;
colormap('lines');
% ax.XTick = 1:6;
ax.XTickLabel = {'1','2','5','10','20'};
% ax.YTick = 0:.2:1;
xlabel('Number of years average');
ylabel('RMS diff.');
ax.YGrid = 'on';
ax.FontSize = 12;
legend({'Assimilated EOF','Original model'}, 'Location', 'northeast');


%% RMSE

   
figure(6);
figuresize(380, 190, 'points');
clf; hold on;
plot(globErrorsNorth(1,:), '.-', 'Color', [0.560181, 0.691569, 0.194885], 'MarkerSize', 20);
plot(globErrorsNorth(2,:), '.-', 'Color', [0.880722, 0.611041, 0.14205], 'MarkerSize', 20);
xlim([0.8, 6.2]);
%ylim([-0.1, 1]);
ax = gca;
ax.XTick = 1:6;
ax.XTickLabel = {'0','1','2','5','10','20'};
%ax.YTick = 0:.2:1;
xlabel('Number of years average');
ylabel('RMS error');
ax.YGrid = 'on';
ax.FontSize = 12;
legend({'Model on data', 'Data on model'}, 'Location', 'northeast');
   
figure(7);
figuresize(380, 190, 'points');
clf; hold on;
plot(globErrorsSouth(1,:), '.-', 'Color', [0.560181, 0.691569, 0.194885], 'MarkerSize', 20);
plot(globErrorsSouth(2,:), '.-', 'Color', [0.880722, 0.611041, 0.14205], 'MarkerSize', 20);
xlim([0.8, 6.2]);
%ylim([-0.1, 1]);
ax = gca;
ax.XTick = 1:6;
ax.XTickLabel = {'0','1','2','5','10','20'};
%ax.YTick = 0:.2:1;
xlabel('Number of years average');
ylabel('RMS error');
ax.YGrid = 'on';
ax.FontSize = 12;
legend({'Model on data', 'Data on model'}, 'Location', 'northeast');

%% Assimilated corr.coeff.
   
figure(8);
figuresize(380, 190, 'points');
clf; hold on;
plot(corrCoeffAssim(1,2:end), '.-', 'Color', [0.560181, 0.691569, 0.194885], 'MarkerSize', 20);
plot(corrCoeffAssim(2,2:end), '.-', 'Color', [0.880722, 0.611041, 0.14205], 'MarkerSize', 20);
plot(corrCoeffNorth(1,2:end), 'Color', [0.6 0.6 0.6], 'MarkerSize', 20);
plot(corrCoeffNorth(2,2:end), '--', 'Color', [0.6 0.6 0.6], 'MarkerSize', 20);
xlim([0.8, 5.2]);
ylim([-0.1, 1]);
ax = gca;
ax.XTick = 1:5;
ax.XTickLabel = {'1','2','5','10','20'};
ax.YTick = 0:.2:1;
xlabel('Number of years average');
ylabel('Corr. coeff.');
ax.YGrid = 'on';
ax.FontSize = 12;
legend({'Assim. on data', 'Data on assim.', 'Model on data', 'Data on model'}, 'Location', 'best');
   


%% Assim. with various sigmas

figure(9);
figuresize(355, 240, 'points');
clf; hold on;
plot(corrCoeffAssim20yearAvg);
plot(corrCoeffAssim10yearAvg);
plot(corrCoeffAssim5yearAvg);
plot(corrCoeffAssim2yearAvg);
plot(corrCoeffAssim1yearAvg);
%plot(corrCoeffAssim0yearAvg);
colorLines = colormap('lines');
[max20v, max20s] = max(corrCoeffAssim20yearAvg); plot(max20s, max20v, '.', 'Color', colorLines(1,:), 'MarkerSize', 20);
[max10v, max10s] = max(corrCoeffAssim10yearAvg); plot(max10s, max10v, '.', 'Color', colorLines(2,:), 'MarkerSize', 20);
[max5v, max5s] = max(corrCoeffAssim5yearAvg); plot(max5s, max5v, '.', 'Color', colorLines(3,:), 'MarkerSize', 20);
[max2v, max2s] = max(corrCoeffAssim2yearAvg); plot(max2s, max2v, '.', 'Color', colorLines(4,:), 'MarkerSize', 20);
[max1v, max1s] = max(corrCoeffAssim1yearAvg); plot(max1s, max1v, '.', 'Color', colorLines(5,:), 'MarkerSize', 20);
%[max0v, max0s] = max(corrCoeffAssim0yearAvg); plot(max0s, max0v, '.', 'Color', colorLines(6,:), 'MarkerSize', 20);
ylim([0.4, 1]);
xlim([0.5 7.5]);
ax = gca;
ax.XTick = 1:7;
ax.XTickLabel = {'0.01','0.05','0.1','0.25','0.5','1','2'};
ax.YTick = 0:.1:1;
xlabel('\sigma');
ylabel('Corr. coeff.');
ax.YGrid = 'on';
ax.FontSize = 12;
legend({'20 year', '10 year', '5 year', '2 year', '1 year'},'Location','eastoutside')

figure(10);
figuresize(355, 240, 'points');
clf; hold on;
%[min0v, min0s] = min(globalErrorsAssim0yearAvg); 
[min1v, min1s] = min(globalErrorsAssim1yearAvg); 
[min2v, min2s] = min(globalErrorsAssim2yearAvg); 
[min5v, min5s] = min(globalErrorsAssim5yearAvg); 
[min10v, min10s] = min(globalErrorsAssim10yearAvg); 
[min20v, min20s] = min(globalErrorsAssim20yearAvg); 

factor = 1.2;

plot(6-factor * min20v+factor * globalErrorsAssim20yearAvg, 'Color', colorLines(1,:));
plot(5-factor * min10v+factor * globalErrorsAssim10yearAvg, 'Color', colorLines(2,:));
plot(4-factor * min5v+factor * globalErrorsAssim5yearAvg, 'Color', colorLines(3,:));
plot(3-factor * min2v+factor * globalErrorsAssim2yearAvg, 'Color', colorLines(4,:));
plot(2-factor * min1v+factor * globalErrorsAssim1yearAvg, 'Color', colorLines(5,:));
%plot(0.2-0.5*min0v+0.5*globalErrorsAssim0yearAvg, 'Color', colorLines(6,:));
%plot(min0s, 0.2, '.', 'Color', colorLines(6,:), 'MarkerSize', 20);
plot(min1s, 2, '.', 'Color', colorLines(5,:), 'MarkerSize', 20);
plot(min2s, 3, '.', 'Color', colorLines(4,:), 'MarkerSize', 20);
plot(min5s, 4, '.', 'Color', colorLines(3,:), 'MarkerSize', 20);
plot(min10s, 5, '.', 'Color', colorLines(2,:), 'MarkerSize', 20);
plot(min20s, 6, '.', 'Color', colorLines(1,:), 'MarkerSize', 20);
ylim([1.8, 6.7]);
ax = gca;
xlim([0.5 7.5]);
ax.XTick = 1:7;
ax.XTickLabel = {'0.01','0.05','0.1','0.25','0.5','1','2'};
ax.YTick = 1:6;
%ax.YTickLabel = {num2str(min20v,2), num2str(min10v,2), num2str(min5v,2), num2str(min2v,2), num2str(min1v,2), num2str(min0v,2)};
ax.YTickLabel = [];
xlabel('\sigma');
ylabel('RMS error');
ax.YGrid = 'on';
ax.FontSize = 12;
legend({'20 year', '10 year', '5 year', '2 year', '1 year'},'Location','eastoutside')


   %%
figure(3);
figuresize(380, 190, 'points');
clf; hold on;
plot(corrCoeffPC2(1,:), '.-', 'Color', [0.368417, 0.506779, 0.709798], 'MarkerSize', 20);
plot(corrCoeffPC2(2,:), '.-', 'Color', [0.922526, 0.385626, 0.209179], 'MarkerSize', 20);
plot(corrCoeffNorth(1,:), '-', 'Color', [0.7 0.7 0.7], 'MarkerSize', 20);
plot(corrCoeffSouth(1,:), '--', 'Color', [0.7 0.7 0.7], 'MarkerSize', 20);
plot([-1,7],[0, 0], 'Color', [0.2 0.2 0.2 0.5]);
xlim([0.8, 6.2]);
ylim([-1, 1]);
ax = gca;
ax.XTick = 1:6;
ax.XTickLabel = {'0','1','2','5','10','20'};
ax.YTick = -1:.25:1;
ax.FontSize = 12;
xlabel('Number of years average');
ylabel('Corr. coeff.');
ax.YGrid = 'on';
legend({'North. Hemisph.', 'South. Hemisph.'});


figure(4);
figuresize(380, 190, 'points');
clf; hold on;
plot(corrCoeffPC2North(1,:), '.-', 'Color', [0.560181, 0.691569, 0.194885], 'MarkerSize', 20);
plot(corrCoeffPC2North(2,:), '.-', 'Color', [0.880722, 0.611041, 0.14205], 'MarkerSize', 20);
plot([-1,7],[0, 0], 'Color', [0.2 0.2 0.2 0.5]);
xlim([0.8, 6.2]);
ylim([-1, 1]);
ax = gca;
ax.XTick = 1:6;
ax.XTickLabel = {'0','1','2','5','10','20'};
ax.YTick = -1:0.25:1;
xlabel('Number of years average');
ylabel('Corr. coeff.');
ax.FontSize = 12;
ax.YGrid = 'on';
legend({'Model on data (PC2)', 'Data on model (PC2)'}, 'Location', 'best');

figure(5);
figuresize(380, 190, 'points');
clf; hold on;
plot(corrCoeffPC2South(1,:), '.-', 'Color', [0.560181, 0.691569, 0.194885], 'MarkerSize', 20);
plot(corrCoeffPC2South(2,:), '.-', 'Color', [0.880722, 0.611041, 0.14205], 'MarkerSize', 20);
plot([-1,7],[0, 0], 'Color', [0.2 0.2 0.2 0.5]);
xlim([0.8, 6.2]);
ylim([-1, 1]);
ax = gca;
ax.XTick = 1:6;
ax.XTickLabel = {'0','1','2','5','10','20'};
ax.YTick = -1:.25:1;
ax.FontSize = 12;
xlabel('Number of years average');
ylabel('Corr. coeff.');
ax.YGrid = 'on';
legend({'Model on data (PC2)', 'Data on model (PC2)'}, 'Location', 'best');

%end