function outp = ByteSize(in)
% BYTESIZE writes the memory usage of the provide variable to the given file
% identifier. Output is written to screen if fid is 1, empty or not provided.


s = whos('in');
outp = sprintf([Bytes2str(s.bytes)]);
disp(outp);
end

function str = Bytes2str(NumBytes)
% BYTES2STR Private function to take integer bytes and convert it to
% scale-appropriate size.

scale = floor(log(NumBytes)/log(1024));
switch scale
    case 0
        str = [sprintf('%.0f',NumBytes) ' B'];
    case 1
        str = [sprintf('%.2f',NumBytes/(1024)) ' kB'];
    case 2
        str = [sprintf('%.2f',NumBytes/(1024^2)) ' MB'];
    case 3
        str = [sprintf('%.2f',NumBytes/(1024^3)) ' GB'];
    case 4
        str = [sprintf('%.2f',NumBytes/(1024^4)) ' TB'];
    case -inf
        % Size occasionally returned as zero (eg some Java objects).
        str = 'Not Available';
    otherwise
       str = 'Over a petabyte!!!';
end
end