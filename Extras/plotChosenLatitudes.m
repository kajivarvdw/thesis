%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Plot the latitudes from 30 deg to 70 deg
% chosen for the data assimilation on EOF's
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dataFolder = '/home/kajivarvdw/Data/';

load([dataFolder 'data.mat']);

L2 = load('coast');
latCoast = L2.lat;
lonCoast = L2.long;


figure(2);
clf;

axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [-90 0 0], 'MapLatLimit', [-90 -10]);
hold on;
latInd = 22:29;
latNew = lat(latInd);
set(gcf, 'Color', 'w');
set(gca, 'Box', 'off');
axis off;

tmp = zeros(32,64);
tmp(latInd,:) = 1;
%geoshow(tmp,[1/5.6250 90 0], 'DisplayType', 'texturemap');
custmap = [[1 1 1]; [1 0.65 0]];
colormap(custmap);
setm(gca, 'GColor', [0.3 0.3 0.3]);
geoshow('landareas.shp', 'EdgeAlpha', 0, 'FaceColor', [0 0 0], 'FaceAlpha', 0.3);
%geoshow(latCoast, lonCoast, 'Color', [1 1 1]);

