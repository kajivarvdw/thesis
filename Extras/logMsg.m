function logMsg( msg, clock )
%LOGMSG Displays a message with timestamp

%elapsedTime = toc;
disp([num2str(toc(clock),'%10.4f') ' sec: ' msg]);


end

