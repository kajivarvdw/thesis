%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Plot the monthly averages used to calculate the anomalies of the HadCRUT dataset
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nc = netcdf.open('../../Data/HadCRUT_absolute.nc');

tempOrig = netcdf.getVar(nc, 0);     % There is a scale factor of 0.01
temp = double(tempOrig) * 0.01;
lat = netcdf.getVar(nc, 1);
lon = netcdf.getVar(nc, 2);
lon(1:36) = lon(1:36) + 360;
time = netcdf.getVar(nc, 3);


for i = [1:12 1:12 1:12 1:12 1:12]
    figure(1); clf;
    imagesc(temp(:,:,i)');
    title(sprintf('Month %d', i));
    drawnow;
end
%%
% Calculate averages for Europe

EuropeLatN = 60;
EuropeLatS = 35;
EuropeLonW = 345;
EuropeLonE = 30;

EuropeLat = find(lat > EuropeLatS & lat < EuropeLatN);
EuropeLon = find(lon > EuropeLonW | lon < EuropeLonE);

anomaliesEurope = zeros(1,12);
for i = 1:12
    inArea = temp(EuropeLon, EuropeLat, i);
    anomaliesEurope(i) = mean(inArea(:));
end

colors = colormap('lines');

figure(2); %clf;
plot(anomaliesEurope, '--', 'DisplayName', 'Europe', 'Color', colors(1,:)); hold on;

% Calculate averages for the Northern Hemisphere

LatN = 90;
LatS = 0;

NHemisphLat = find(lat > LatS & lat < LatN);
NHemisphLon = 1:length(lon);

anomaliesNH = zeros(1,12);
for i = 1:12
    inArea = temp(NHemisphLon, NHemisphLat, i);
    anomaliesNH(i) = mean(inArea(:));
end

plot(anomaliesNH, '--', 'DisplayName', 'Northern Hemisphere', 'Color', colors(2,:)); 

% Calculate averages for the Southern Hemisphere

LatN = 0;
LatS = -90;

SHemisphLat = find(lat > LatS & lat < LatN);
SHemisphLon = 1:length(lon);

anomaliesSH = zeros(1,12);
for i = 1:12
    inArea = temp(SHemisphLon, SHemisphLat, i);
    anomaliesSH(i) = mean(inArea(:));
end

plot(anomaliesSH, '--', 'DisplayName', 'Southern Hemisphere', 'Color', colors(3,:)); 



%legend('show');

ylabel('Temperature (°C)');
xlabel('Month');
axis([1 12 -5 35]);
