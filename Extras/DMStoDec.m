function outp = DMStoDec(str)
    sgn = str(1);
    spl = strsplit(str(2:end),':');
    if (strcmp(sgn,'+'))
        sign = 1;
    else
        sign = -1;
    end
    outp = sign * (str2double(spl{1}) + str2double(spl{2})/60 + str2double(spl{3})/3600);
end