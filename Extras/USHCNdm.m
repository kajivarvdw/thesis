function [ numdays_list ] = USHCNdm( dm_list )
%USHCNDM Changes a letter quality notation of the US HCN dataset to a number of missing days
%   Data measurement: A - H or a - i(q): number of days
%   missing (A/a = 1, B/b = 2, ...) for calculation of monthly average
%   E/I(Q)/.: missing value, should be nan

% We assume an average of 30 days per month

numdays_list = 30 * ones(size(dm_list));

numdays_list(dm_list == 'a' | dm_list == 'A') = 29;
numdays_list(dm_list == 'b' | dm_list == 'B') = 28;
numdays_list(dm_list == 'c' | dm_list == 'C') = 27;
numdays_list(dm_list == 'd' | dm_list == 'D') = 26;
numdays_list(dm_list == 'e' | dm_list == 'E') = 25;
numdays_list(dm_list == 'f' | dm_list == 'F') = 24;
numdays_list(dm_list == 'g' | dm_list == 'G') = 23;
numdays_list(dm_list == 'h' | dm_list == 'H') = 22;
numdays_list(dm_list == 'q') = 21;

numdays_list(dm_list == 'Q'  | dm_list == 'p') = 0;

end

