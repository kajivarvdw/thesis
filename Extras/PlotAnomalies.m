function PlotAnomalies
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Plot the monthly averages used to calculate the anomalies
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dataset = 'hadcrut';    % hadcrut, eca, ushcn. Default: hadcrut

load('../../Data/data.mat');

if (strcmp(dataset,'eca'))
    load('../../Data/eca/averagesRefAnomaliesECA.mat');
    name = 'ECA';
elseif (strcmp(dataset,'ushcn'))
    load('../../Data/ushcn/averagesRefAnomaliesUSHCN.mat');
    name = 'USHCN';
else
    load('../../Data/anomalies.mat');
    name = '';
end

clf;

%% Plot for Europe

if (~strcmp(dataset,'ushcn'))

EuropeLatN = 60;
EuropeLatS = 35;
EuropeLonW = 345;
EuropeLonE = 30;

EuropeLat = find(lat > EuropeLatS & lat < EuropeLatN);
EuropeLon = find(lon > EuropeLonW | lon < EuropeLonE);


plot(calcAnomaliesAverage(averages, EuropeLat, EuropeLon), 'DisplayName', ['Europe ' name]); hold on;

end

%% Northern Hemisphere

LatN = 90;
LatS = 0;

NHemisphLat = find(lat > LatS & lat < LatN);
NHemisphLon = 1:length(lon);


plot(calcAnomaliesAverage(averages, NHemisphLat, NHemisphLon), 'DisplayName', ['North. Hemisph. ' name]); hold on;



%% Southern Hemisphere

if (~strcmp(dataset,'eca') && ~strcmp(dataset,'ushcn'))
    LatN = 0;
    LatS = -90;

    SHemisphLat = find(lat > LatS & lat < LatN);
    SHemisphLon = 1:length(lon);


    plot(calcAnomaliesAverage(averages, SHemisphLat, SHemisphLon), 'DisplayName', ['South. Hemisph. ' name]); hold on;
    
end

legend('show','Location','best');

ylabel('Temperature (°C)');
xlabel('Month');
axis([1 12 -5 35]);
%title('Reference temperatures for anomalies');
ax = gca;
ax.XTick = 1:12;
ax.XGrid = 'on';

end



function output = calcAnomaliesAverage(averages, latVal, lonVal)
chosenData = averages(lonVal, latVal, :);

averaged = zeros(1,12);
for i=1:12
    tmp = chosenData(:,:,i);
    tmp = tmp(:);
    averaged(i) = mean(tmp(~isnan(tmp)));
end

output = averaged;

end



