function PlotData (moment, projection, diff)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Plot data using various projections
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Import data


dataFolder = '/home/kajivarvdw/Data/';
load([dataFolder 'data.mat']);

numFig = 2;
if (diff)
    numFig = 3;
end


%% Plot using world map projection

clf;

if (strcmp(projection,'world'))
    load coast;
    subplot(numFig,1,1);
    axesm eckert4;
    framem; gridm; axis off;

    subplot(numFig,1,2);
    axesm eckert4;
    framem; gridm; axis off;

    subplot(numFig,1,1);
    geoshow(dataModel(:,:,moment)',[1/5.6250 90 0], 'DisplayType', 'texturemap');
    caxis([-10 10]);
    geoshow(lat, long);

    subplot(numFig,1,2);
    geoshow(dataMeasurement(:,:,moment)',[1/5.6250 90 0], 'DisplayType', 'texturemap');
    caxis([-10 10]);
    geoshow(lat, long);
    
    if (diff)
        subplot(numFig,1,3);
        geoshow(dataMeasurement(:,:,moment)'-dataModel(:,:,moment)',[1/5.6250 90 0], 'DisplayType', 'texturemap');
        caxis([-10 10]);
        title('Difference: measurement - model');
        geoshow(lat, long);
    end
        
    
else
    subplot(numFig,1,1);
    imagesc(flipud(dataModel(:,:,moment)'), [-10 10]);
    subplot(numFig,1,2);
    imagesc(flipud(dataMeasurement(:,:,moment)'), [-10 10]);
    
    if (diff)
        subplot(numFig,1,3);
        imagesc(dataMeasurement(:,:,moment)'-dataModel(:,:,moment)', [-10 10]);
        title('Difference: measurement - model');
    end
    
end

drawnow;
    