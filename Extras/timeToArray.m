function outp = timeToArray( time )
    outp = zeros(3,1);
    outp(1) = int32(time / 10000);
    outp(2) = int32(mod(time / 100, 100));
    outp(3) = int32(mod(time, 100));
end