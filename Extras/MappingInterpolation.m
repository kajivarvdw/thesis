%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Comparison of remapping and bilinear interpolation
%
% - Import latitudes/longitudes
% - Create subgridpoints
% - Calculate average
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
clf;

fileModel = '../../Data/P1_ICLMALL5m3_ts.nc';
%fileModel = '../../Data/UnpertFy850Py1500_1_ts.nc';
fileMeasurement = '../../Data/HadCRUT.4.4.0.0.median.nc';

ncModel = netcdf.open(fileModel);
ncMeasurement = netcdf.open(fileMeasurement);

latMeasurement     = netcdf.getVar(ncMeasurement, 0);
lonMeasurement     = netcdf.getVar(ncMeasurement, 1);


latModel     = netcdf.getVar(ncModel, 0);
lonModel     = netcdf.getVar(ncModel, 1);

sizeLon = length(lonModel);
sizeLat = length(latModel);

clear ncModel ncMeasurement fileModel fileMeasurement


%% Uniform longitudes (0 to 360 degrees)

lonMeasurement(lonMeasurement < 0) = lonMeasurement(lonMeasurement < 0) + 360;
lonMeasurement = [lonMeasurement(37:end); lonMeasurement(1:36)];

%% Create grids

[gridLatModel, gridLonModel] = meshgrid(latModel, lonModel);

[gridLatMeas, gridLonMeas] = meshgrid(latMeasurement, lonMeasurement);

subLatMeas = -89.5:1:89.5;
subLonMeas = 0.5:1:359.5;
[gridSubLat, gridSubLon] = meshgrid(subLatMeas, subLonMeas);

figure(1);
plot(gridSubLon', gridSubLat', 'g.');
hold on;
plot(gridLonMeas', gridLatMeas', 'bo');
plot(gridLonModel', gridLatModel', 'r.');  % The red dots are the grid points of the model, which we want to project the measurements on.
%hold off;

modelLinesVertX = [2.8125:5.626:357.1875; 2.8125:5.626:357.1875];
modelLinesVertY = ones(2, length(modelLinesVertX));
modelLinesVertY(1,:) = -90;
modelLinesVertY(2,:) = 90;

plot(modelLinesVertX, modelLinesVertY, 'color', [0.5 0.5 0.5]);

modelLinesHorizY = [latModel(1)+5.5329/2:5.5329:latModel(end); latModel(1)+5.5329/2:5.5329:latModel(end)];
modelLinesHorizX = ones(2, length(modelLinesHorizY));
modelLinesHorizX(1,:) = 0;
modelLinesHorizX(2,:) = 360;

plot(modelLinesHorizX, modelLinesHorizY, 'color', [0.5 0.5 0.5]);

hold off;



