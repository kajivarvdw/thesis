function [ timeIndex ] = timeToIndex( year, month )
%TIMETOINDEX Gets an index from 1 to 1788 given a year/month combination

% Min: 1850 1
% Max: 1998 12

if (year < 1850 || year > 1998 || month < 0 || month > 12)
    timeIndex = -1;
else
    timeIndex = int32((year - 1850) * 12 + month);
end

end

