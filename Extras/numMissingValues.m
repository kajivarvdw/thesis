%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Calculate percentage of missing values
% in HadCRUT measurements dataset.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load('../../Data/data.mat');

numPoints = length(lat) * length(lon);
numTimes = length(time);

percentages = zeros(1,numTimes);
for i = 1:numTimes
    dataFlat = dataMeasurement(:,:,i);
    dataFlat = dataFlat(:);
    percentages(i) = length(find(isnan(dataFlat))) / numPoints;
end

figure(1);
plot(1-percentages);
axis([0,1800,0,1])
ax = gca;
ax.XTick = 0:300:1800;
ax.XTickLabel = {'1850','1875','1900','1925','1950','1975','2000'};
ax.YTick = 0:.2:1;
ax.YTickLabel = {'0%', '20%', '40%', '60%', '80%', '100%'};
xlabel('Year');
ylabel('Global coverage');
%title('Coverage in percentage of HadCRUT dataset');

L2 = load('coast');
latCoast = L2.lat;
lonCoast = L2.long;


%% Plot on map

percentagesGlobe = 1-sum(isnan(dataMeasurement), 3) ./ numTimes;

% North
figure(2);
clf;
axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [90 0 0], 'MapLatLimit', [15 90]);


latNew = lat(17:end);
lonNew = [lon(33:end)' - 360 lon(1:32)']';
tmp = percentagesGlobe(:,17:end)';
tmp = [tmp(:,33:end) tmp(:,1:32)];
lonNewTmp = lonNew;
lonNewTmp(end) = 180; 
hold on;
contourfm(double(latNew), double(lonNewTmp), tmp, 12, 'LineStyle', 'none');
setm(gca, 'GColor', [0.3 0.3 0.3]);
%geoshow('landareas.shp', 'EdgeAlpha', 0, 'FaceColor', [1 1 1], 'FaceAlpha', 0.3);
geoshow(latCoast, lonCoast, 'Color', [1 1 1]);
%hotmap = hot;
load('whitegreenmap','whitegreenmap');
colormap(whitegreenmap);
h = colorbar;
set(h, 'YTick', 0:0.2:1);
caxis([0,1]);
ax = gca;
cpos = h.Position;
axpos = ax.Position;
cpos(3) = 0.5*cpos(3);
cpos(1) = 1.0*cpos(1);
ax.Position = axpos;
h.Position = cpos;
set(h,'fontsize',12);
axis off;
set(gcf, 'Color', 'w');   

%% South

figure(3);
clf;
axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [-90 0 0], 'MapLatLimit', [-90 -10]);


latNew = lat(1:16);
lonNew = [lon(33:end)' - 360 lon(1:32)']';
tmp = percentagesGlobe(:,1:16)';
tmp = [tmp(:,33:end) tmp(:,1:32)];
lonNewTmp = lonNew;
lonNewTmp(end) = 180; 
hold on;
contourfm(double(latNew), double(lonNewTmp), tmp, 12, 'LineStyle', 'none');
setm(gca, 'GColor', [0.3 0.3 0.3]);
%geoshow('landareas.shp', 'EdgeAlpha', 0, 'FaceColor', [0 0 0], 'FaceAlpha', 0.3);
geoshow(latCoast, lonCoast, 'Color', [1 1 1]);
%hotmap = hot;
colormap(whitegreenmap);
h = colorbar;
set(h, 'YTick', 0:0.2:1);
caxis([0,1]);
ax = gca;
cpos = h.Position;
axpos = ax.Position;
cpos(3) = 0.5*cpos(3);
cpos(1) = 1.0*cpos(1);
ax.Position = axpos;
h.Position = cpos;
set(h,'fontsize',12);
axis off;
set(gcf, 'Color', 'w');   