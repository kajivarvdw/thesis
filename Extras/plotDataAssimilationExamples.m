%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Plot the latitudes from 30 deg to 70 deg
% chosen for the data assimilation on EOF's
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dataFolder = '/home/kajivarvdw/Data/';

load([dataFolder 'data.mat']);
load([dataFolder 'data_assimilated_sigma0.01_20yearAverage.mat']);

%%

% Interesting ones:
% - 0yearAverage:  1620
% - 1yearAverage:  125
% - 2yearAverage:  46
% - 5yearAverage:  17 (mwa)
% - 10yearAverage: 9 (mwa)
% - 20yearAverage: 1620
moment = 5;

L2 = load('coast');
latCoast = L2.lat;
lonCoast = L2.long;

tmpA = dataAssimilated(:,:,moment);
tmpB = dataModel(:,:,moment);
tmpO = dataObservations(:,:,moment);

maxV1 = max([max(tmpA(:)), max(tmpB(:)), max(tmpO(:))]);
minV1 = min([min(tmpA(:)), min(tmpB(:)), min(tmpO(:))]);

maxV = 0.9*max(abs([maxV1, minV1]));
minV = -1.15*maxV;


figure(1);
figuresize(780, 270, 'points');
set(gcf, 'Color', 'w');
colormap('jet');
cmap = colormap;
cmap(1,:) = [1 1 1];

coastColor = [0.2 0.2 0.2];

clf;

% Background field
axB = subplot(1,3,1);
axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [90 0 0], 'MapLatLimit', [11 90]);
hold on;
geoshow(tmpB',[1/5.6250 90 0], 'DisplayType', 'texturemap');
caxis([minV maxV]);
colormap(cmap);
setm(gca, 'GColor', [0.3 0.3 0.3]);
%geoshow('landareas.shp', 'EdgeAlpha', 0, 'FaceColor', [0 0 0], 'FaceAlpha', 0.3);
%axB.Position(3) = 0.3;
geoshow(latCoast, lonCoast, 'Color', coastColor);
title('Background field', 'Interpreter', 'LaTeX');
axB.FontSize = 12;
axB.XLim = [-1.5 1.5];
axB.YLim = [-1.5 1.5];
axB.Box = 'off';
axB.XColor = [1 1 1];
axB.YColor = [1 1 1];

%%

% Observation field
axO = subplot(1,3,2);
axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [90 0 0], 'MapLatLimit', [11 90]);
hold on;
geoshow(tmpO',[1/5.6250 90 0], 'DisplayType', 'texturemap');
colormap(cmap);
caxis([minV maxV]);
setm(gca, 'GColor', [0.3 0.3 0.3]);
%geoshow('landareas.shp', 'EdgeAlpha', 0, 'FaceColor', [0 0 0], 'FaceAlpha', 0.3);
%axO.Position(3) = 0.3;
geoshow(latCoast, lonCoast, 'Color', coastColor);
title('Observation field', 'Interpreter', 'LaTeX');
axO.FontSize = 12;
axO.XLim = [-1.5 1.5];
axO.YLim = [-1.5 1.5];
axO.Box = 'off';
axO.XColor = [1 1 1];
axO.YColor = [1 1 1];

%%

% Assimilated field
axA = subplot(1,3,3);
axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [90 0 0], 'MapLatLimit', [11 90]);
hold on;
geoshow(tmpA',[1/5.6250 90 0], 'DisplayType', 'texturemap');
colormap(cmap);
caxis([minV maxV]);
setm(gca, 'GColor', [0.3 0.3 0.3]);
%geoshow('landareas.shp', 'EdgeAlpha', 0, 'FaceColor', [0 0 0], 'FaceAlpha', 0.3);
%axA.Position(3) = 0.3;
geoshow(latCoast, lonCoast, 'Color', coastColor);
title('Assimilated field', 'Interpreter', 'LaTeX');
axA.FontSize = 12;
axA.XLim = [-1.5 1.5];
axA.YLim = [-1.5 1.5];
axA.Box = 'off';
axA.XColor = [1 1 1];
axA.YColor = [1 1 1];

axB.Position(3) = 0.25;
axO.Position(3) = 0.25;
axA.Position(3) = 0.25;


%% Total difference between background and assimilated



n = size(dataModel, 3);
p = size(dataModel, 1) * size(dataModel, 2);

tmpReshapedA = reshape(dataAssimilated, [p n])';
tmpReshapedB = reshape(dataModel, [p n])';

diff = rms(tmpReshapedA' - tmpReshapedB'); %./ sqrt(sum(squeeze(sum((dataModel).^2,1)),1));
%diff = mean(abs(tmpReshapedA' - tmpReshapedB'));
fprintf('nYearAvg: %d, sigma: %f, mean diff: %f\n', nYearAverage, sigmaModel, mean(diff));
figure(3); hold on;
figuresize(380, 190, 'points');

if (nYearAverage > 0.5)
    x=1850+nYearAverage:nYearAverage:1999;
    plot(x,diff);
else
    x=1:length(time);
    plot(x,diff);
    set(gca,'XTick',1:300:1801)
    set(gca,'XTickLabel',{'1850', '1875', '1900', '1925', '1950', '1975','2000'})
    axis([1 1801 0 1.1*max(diff)]);
end
set(gca,'FontSize',12);
set(gca,'YGrid','on');

ylabel('RMS diff. (^o C)');
xlabel('Year');

% Interesting: periodicity of 12 months
if (nYearAverage < 0.5)
    test = diff;
    diffMonthly = zeros(12,1);
    for i = 1:12
        diffMonthly(i) = mean(test(i:12:end));
    end
    figure(5);
    figuresize(380, 130, 'points');
    plot(diffMonthly)
    set(gca,'xTick',1:12)
    set(gca,'xTickLabel',{'Jan.', '', 'Mar.', '', 'May', '','Jul.','','Sep.','','Nov.',''})
    set(gca,'FontSize',12);
    set(gca,'XGrid','on');
    axis([1 12 0 1.1*max(diffMonthly)]);
    ylabel('Avg.diff. (^o C)');

end

