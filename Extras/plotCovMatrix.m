%%% Plots the covariance matrix of the background field, along with some zoomed in section

figure(1);
clf;
sigma = 0.2;
rho = 0.5;
Porig = CreateCovariance(sigma, rho);
P = Porig.^0.5;
maxP = max(P(:));
colormap(flipud(hot));
set(gcf, 'Color', 'w');
figuresize(500, 200, 'points');

subplot(3,3,[1,2,4,5,7,8]);
hold on;
imagesc(P(1:150,1:150));
caxis([0,maxP]);
axis([0 150 0 150]);
axis ij;
h = colorbar('westoutside','Ticks',[0, rho^3*sigma^2, rho^2*sigma^2, rho*sigma^2, sigma^2].^0.5, 'TickLabels',{'0', '\rho^3 \sigma^2', '\rho^2 \sigma^2', '\rho \sigma^2', '\sigma^2'});
set(h,'FontSize',13);
cpos = h.Position;
ax = gca;
axpos = ax.Position;
cpos(3) = 0.5*cpos(3);
cpos(1) = 0.7*cpos(1);
axpos(1) = 0.85*axpos(1);
ax.Position = axpos;
h.Position = cpos;
set(gca,'box','on')

color = [0.75 0.75 0.75];

rectangle('Position', [0 0 150 150]);

rectangle('Position', [126 0 11 11],'EdgeColor',color);
rectangle('Position', [60 60 11 11],'EdgeColor',color);
rectangle('Position', [60 124 11 11],'EdgeColor',color);

line([137 150], [0 0],'Color',color);
line([137 150], [11 39],'Color',color);

line([71 150], [60 54],'Color',color);
line([71 150], [71 94],'Color',color);

line([71 150], [124 110],'Color',color);
line([71 150], [135 145],'Color',color);


subplot(3,3,3);
range1x = 126:137;
range1y = 1:11;
imagesc(range1x, range1y, P(range1y,range1x));
caxis([0,maxP]);
ax = gca;
ax.YTick = [1 5 10];

subplot(3,3,6);
range2x = 60:71;
range2y = 60:71;
imagesc(range2x, range2y, P(range2y,range2x));
caxis([0,maxP]);

subplot(3,3,9);
range3x = 60:71;
range3y = 124:135;
imagesc(range3x, range3y, P(range3y,range3x));
caxis([0,maxP]);