function [ C ] = FahrenheitToCelsius( F )
%FAHRENHEITTOCELSIUS Transforms degrees Fahrenheit to degrees Celsius

C = (F - 32) * 5/9;

end

