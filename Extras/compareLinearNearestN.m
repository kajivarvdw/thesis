%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Comparison of nearest neighbour interpolation and combination bilinear / linear interpolation
%
% - Import both datasets
% - Plot average, max, min difference
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Import data

load '../../Data/data.mat';                % Bilinear / linear interpolation
dataMeasurementLinear = dataMeasurement;
load '../../Data/data_nearestn.mat';       % Nearest neighbour interpolation

%% Projected plot

load coast;
figure(4);
clf;
colormap(flipud(hot));
axesm eckert4;
framem; gridm off; axis off;
colorbar;
        figuresize(410, 190, 'points');
differences = (abs(dataMeasurementLinear(:,:,1501) - dataMeasurement(:,:,1501)))';
%differences(isnan(differences)) = 2.5;
geoshow(differences,[1/5.6250 90 0], 'DisplayType', 'texturemap');
caxis([0 1.5]);
geoshow(lat, long, 'Color', [.7 .7 .7]);
            drawnow;
%            print('test', '-dpdf');

%% Average, max, min difference

totalTmp = abs(dataMeasurementLinear - dataMeasurement);
totalTmp = totalTmp(:);
totalAbsAverage = mean(totalTmp(~isnan(totalTmp)))

%%

figure(1);
clf;

averageDiff = zeros(1, length(time));
minDiff = zeros(1, length(time));
maxDiff = zeros(1, length(time));
minLinear = zeros(1, length(time));
minNearestN = zeros(1, length(time));
maxLinear = zeros(1, length(time));
maxNearestN = zeros(1, length(time));
numNan1 = zeros(1, length(time));
numNan2 = zeros(1, length(time));

for i = 1:length(time)
    flat1 = dataMeasurementLinear(:,:,i);
    flat1 = flat1(:);
    flat2 = dataMeasurement(:,:,i);
    flat2 = flat2(:);
    existingValues = not(isnan(flat1) | isnan(flat2));
    averageDiff(i) = mean(flat1(existingValues) - flat2(existingValues));
    minDiff(i) = min(flat1(existingValues) - flat2(existingValues));
    maxDiff(i) = max(flat1(existingValues) - flat2(existingValues));
    numNan1(i) = length(find(isnan(flat1)));
    numNan2(i) = length(find(isnan(flat2)));
    minLinear(i) = min(flat1(existingValues));
    maxLinear(i) = max(flat1(existingValues));
    minNearestN(i) = min(flat2(existingValues));
    maxNearestN(i) = max(flat2(existingValues));
end


figure(1);
plot(averageDiff); hold on;
plot(maxDiff);
plot(minDiff);
hold off;
title('Differences between (bi)linear interpolation and nearest neighbour interpolation');
legend(['Average (total average: ' num2str(mean(averageDiff)) ')'], ['Maximum difference (average: ' num2str(mean(maxDiff)) ')'], ['Minimum difference (average: ' num2str(mean(minDiff)) ')']);


figure(2);
hold on;
plot(minLinear);
plot(minNearestN);
plot(maxLinear);
plot(maxNearestN);
hold off;
legend('minLinear', 'minNearestN', 'maxLinear', 'maxNearestN');

figure(3);
hold on;
plot(minLinear - minNearestN);
hold off;
legend('minLinear - minNearestN');

%figure(2);
%plot(numNan1); hold on;
%plot(numNan2); hold off;
%title('Number of NaN values');
%legend('(Bi)linear interpolation', 'Nearest neighbour interpolation');
