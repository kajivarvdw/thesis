%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Accuracy analysis of LOVECLIM climate model in comparison to HadCRUT measurements
%
% Bachelor thesis Kaj-Ivar van der Wijst
% Under supervision of Svetlana Dubinkina, CWI, Amsterdam
% November 2015
%
% Assimilating extra data to the model
% ------------------------------------
%
% - Import model and data
% - Create constant model error covariance matrix P
% - Perform data assimilation for every timestep:
%       a. Create observation matrix H
%       b. Create heteroskedastic error cov. matrix R for data
%       c. Calculate Kalman filter
%       d. Perform actual data assimilation
% - Save generated output. This contains, among others, these variables:
%       * dataAssimilated       (analyzed field, 64x32xn matrix)
%       * dataObservations      (observation field, 64x32xn matrix)
%       * dataModel             (background field, 64x32xn matrix)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dataFolder = GetDataFolder();


for sigmaModel = [0.2]      % [2 1 0.5 0.25 0.1 0.05 0.01]
for nYearAverage = [10]      % [1 2 5 10 20]
    
outputfile = [dataFolder 'assimilated/data_assimilated_sigma' num2str(sigmaModel) '_' num2str(nYearAverage) 'yearAverage.mat'];

useECA = true;
useUSHCN = true;

saveFile = true;
usePreCombinedDatasets = true;


%% Import data

clock = tic;

logMsg(['Starting assimilation with sigma = ' num2str(sigmaModel) '...'], clock);

logMsg('Importing model...', clock);
load([dataFolder 'data.mat']);

numLat = length(lat);


if (~usePreCombinedDatasets)

    logMsg('Importing ECA data...', clock);
    load([dataFolder 'eca/data_eca.mat']);
    logMsg('Importing USHCN data...', clock);
    load([dataFolder 'ushcn/data_ushcn.mat']);

    % Combine various data sources

    dataObservations = zeros(size(dataModel));
    existingValues = nan(size(dataModel));

    numDailyMeasObs = zeros(size(dataModel), 'int32');

    % This only works if there are no overlapping measurements
    if (useECA)
        numDailyMeasObs = numDailyMeasObs + numDailyMeasECA;
        dataObservations(~isnan(dataECA)) = dataECA(~isnan(dataECA));
        existingValues(~isnan(dataECA)) = 0;
    end
    if (useUSHCN)
        numDailyMeasObs = numDailyMeasObs + numDailyMeasUSHCN;
        dataObservations(~isnan(dataUSHCN)) = dataUSHCN(~isnan(dataUSHCN));
        existingValues(~isnan(dataUSHCN)) = 0;
    end
    
    save([dataFolder 'data_eca_ushcn_combined.mat'], 'numDailyMeasObs', 'dataObservations', 'existingValues');
    
else
    load([dataFolder 'data_eca_ushcn_combined.mat']);
end

% Set non-existing values back to nan
dataObservations(isnan(existingValues)) = nan;

n = size(dataModel, 3);
p = size(dataModel, 1) * size(dataModel, 2);

% Reshape data and model to n x p matrix, with n=1788 time intervals and p=2048 spatial grid points
matrixModelOrig = reshape(dataModel, [p n])';
matrixDataOrig = reshape(dataObservations, [p n])';
matrixDailyMeasOrig = reshape(numDailyMeasObs, [p n])';

% Plot dimension of background field (blue) and observation field (red)

figure(20); clf;
hold on;
plot(sum(~isnan(matrixModelOrig),2));
plot(sum(~isnan(matrixDataOrig),2));

figuresize(380, 190, 'points');
legend({'$p = \dim({\bf{w}}^b_k)$','$M = \dim({\bf{w}}^o_k)$'},'Interpreter','LaTeX','Location','best')
set(gca,'XTick',0:12*50:3*12*50);
set(gca,'XtickLabel', {'1850','1900','1950','2000'});
set(gca,'XLim',[0 3*12*50]);
set(gca,'YTick',0:1000:2000);
set(gca,'FontSize',12);


%% Calculate n year average (if necessary)

if (nYearAverage > 0)
    
    % We ignore every additional months which doesn't fit into the n-year bins

    nMonthAverage = nYearAverage * 12;
    totalBins = int16(floor(n / nMonthAverage));

    averagingMatrix = zeros(totalBins, n);

    for b = 0:totalBins-1
        averagingMatrix(b+1, b*nMonthAverage+1:(b+1)*nMonthAverage) = 1;
    end
    
    % Take average of model
    matrixModel = averagingMatrix * matrixModelOrig / (nMonthAverage);
    
    % Take average of measurements
    matrixData = matrixDataOrig;        matrixData(isnan(matrixDataOrig)) = 0;
    numExistingMonths = averagingMatrix * (~isnan(matrixDataOrig));
    matrixData = (averagingMatrix * matrixData) ./ (numExistingMonths);
    % Only keep values with at least two months of data per year
    matrixData(numExistingMonths < 2 * nYearAverage) = nan;
    
    % Number of daily measurements are added together
    matrixDailyMeas = int32(averagingMatrix * double(matrixDailyMeasOrig));
    
    n = double(totalBins);
    
    dataObservations = reshape(matrixData', [64 32 n]);
    dataModel = reshape(matrixModel', [64 32 n]);
    
    
else
    matrixModel = matrixModelOrig;
    matrixData = matrixDataOrig;
    matrixDailyMeas = matrixDailyMeasOrig;
end



%% Perform data assimilation

logMsg('Performing data assimilation...', clock);

% Create matrix with analyzed field ('improved guess')
matrixAnalyzed = zeros(n, p);

% Create constant model error covariance matrix P
if (nYearAverage > 0)
    sigma = sigmaModel / sqrt(12 * nYearAverage);
else
    sigma = sigmaModel;
end

rho = 0.5;
P = CreateCovariance(sigma, rho);

% Perform data assimilation for every timestep:
timing = int16(n / 10);

for i = 1:n
    
    tmpObs = matrixData(i,:)';
    tmpDailyMeas = matrixDailyMeas(i,:)';
    
    existingObs = find(~isnan(tmpObs));         % Positions of existing observations
    
    xObs = tmpObs(existingObs);                 % Observation vector
    xModel = matrixModel(i,:)';
    
    N = length(existingObs);
    
    % a. Create observation matrix H 
    
    H = zeros(N, p);
    for j = 1:N
        H(j, existingObs(j)) = 1;
    end
    
    % b. Create heteroskedastic error cov. matrix R for data
    
    sigmaDailyMeasurement = 0.78;              % Assume every station has a measurement uncertainty of 1 degree
    varPerMeasurement = sigmaDailyMeasurement ^ 2;  % = sigma^2
    numDailyMeas = tmpDailyMeas(existingObs);   % The uncertainty is sigma / sqrt(N), so we divide the variance by the number of stations
    
    vectorDiag = (varPerMeasurement * ones(N, 1)) ./ double(numDailyMeas);
    R = diag(vectorDiag);
    
    % c. Calculate Kalman filter
    
    K = (P * (H')) / (H * P * H' + R);
    
    % d. Perform actual data assimilation
    
    xAnalyzed = xModel + K * (xObs - H * xModel);
    matrixAnalyzed(i, :) = xAnalyzed;
    
    % Log every 10%
    if (mod(i,timing) == 0)
        logMsg(['Data assimilation: finished ' num2str(10*i/timing) '%'],clock)
    end
    
end

dataAssimilated = reshape(matrixAnalyzed', [64 32 n]);


%% Save assimilated field

if (saveFile)
    logMsg(['Saving file to ' outputfile '...'], clock);

    save(outputfile, 'dataAssimilated', 'dataObservations', 'dataModel', 'useECA', 'useUSHCN', 'nYearAverage', 'matrixDailyMeas', 'sigmaModel');
end

%% Done

end
end

logMsg('Done.', clock);
