function [ covarianceMatrix ] = CreateCovariance( sigma, rho, customP )
%CREATECOVARIANCE Creates a covariance matrix for the model
%   
%           3 
%       2   1   2 
%   3   1   X   1   3
%       2   1   2
%           3 
%
%   For every grid point, we assign the following correlations:
%       X: sigma^2
%       1: rho * sigma^2
%       2: rho^2 * sigma^2
%       3: rho^3 * sigma^2

if (nargin == 2)
    p = 64 * 32;
else
    p = customP;
end

covarianceMatrix = zeros(p, p);

for x = 1:p
    
    if (length(sigma) == p)
        v = sigma(x)^2;
    else
        v = sigma(1)^2;
    end
    
    v1 = rho * v;
    v2 = rho^2 * v;
    v3 = rho^3 * v;
    
    if (isinf(v) || v > 1e8)
        v1 = 0;
        v2 = 0;
        v3 = 0;
    end
    
    a1 = x - 1;
    b1 = x + 64;
    c1 = x + 1;
    d1 = x - 64;
    
    a2 = x + 63;
    b2 = x + 65;
    c2 = x - 63;
    d2 = x - 65;
    
    a3 = x - 2;
    b3 = x + 128;
    c3 = x + 2;
    d3 = x - 128;
    
    % Boundary conditions: periodic longitudes
    if (mod(x, 64) == 1)        % Top boundary
        a1 = x + 63;
        a2 = x + 127;
        d2 = x - 1;
        a3 = x + 62;
    elseif (mod(x, 64) == 2)
        a3 = x + 62;
    elseif (mod(x, 64) == 0)    % Bottom boundary
        c1 = x - 63;
        b2 = x + 1;
        c2 = x - 127;
        c3 = x - 62;
    elseif (mod(x, 64) == 63)
        c3 = x - 62;
    end
    
    % Fill in the variance
    covarianceMatrix(x, x) = v;
    
    % Fill in the 1-covariances
    if (a1 > 0 && a1 <= p), covarianceMatrix(numInMatrix(x, a1)) = v1; end;
    if (b1 > 0 && b1 <= p), covarianceMatrix(numInMatrix(x, b1)) = v1; end;
    if (c1 > 0 && c1 <= p), covarianceMatrix(numInMatrix(x, c1)) = v1; end;
    if (d1 > 0 && d1 <= p), covarianceMatrix(numInMatrix(x, d1)) = v1; end;
    
    % Fill in the 2-covariances
    if (a2 > 0 && a2 <= p), covarianceMatrix(numInMatrix(x, a2)) = v2; end;
    if (b2 > 0 && b2 <= p), covarianceMatrix(numInMatrix(x, b2)) = v2; end;
    if (c2 > 0 && c2 <= p), covarianceMatrix(numInMatrix(x, c2)) = v2; end;
    if (d2 > 0 && d2 <= p), covarianceMatrix(numInMatrix(x, d2)) = v2; end;
    
    % Fill in the 3-covariances
    if (a3 > 0 && a3 <= p), covarianceMatrix(numInMatrix(x, a3)) = v3; end;
    if (b3 > 0 && b3 <= p), covarianceMatrix(numInMatrix(x, b3)) = v3; end;
    if (c3 > 0 && c3 <= p), covarianceMatrix(numInMatrix(x, c3)) = v3; end;
    if (d3 > 0 && d3 <= p), covarianceMatrix(numInMatrix(x, d3)) = v3; end;
    
end

    function num = numInMatrix(row, col)
        num = [row + (col - 1) * p, col + (row - 1) * p];
    end

end

