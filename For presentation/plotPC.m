%% Plot a PC for the presentation

dataFolder = '/home/kajivarvdw/Data/';
inputfile = [dataFolder 'EOFsPCs/EOFsPCs_5yearAverage_north.mat'];
load(inputfile);

x=1850+nYearAverage:nYearAverage:1999;

for i = 1:5
    figure(i);
    clf;
    figuresize(190, 100, 'points');
    plot([1850 1999],[0 0],'Color',[0.7 0.7 0.7],'LineWidth',1.6);
    set(gca,'LineWidth',1.35)
    hold on; 
    p1 = plot(x,dataPCs(:,i)','Color',[0.368417, 0.506779, 0.709798],'LineWidth',1.8);
    set(gcf, 'Color', [250/255, 250/255, 250/255]);
end