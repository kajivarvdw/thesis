dataFolder = '/home/kajivarvdw/Data/';

load([dataFolder 'data.mat']);

numYears = 20;
showNumPoints = false;



%% Europe

% First calculate which points belong to Europe
EuropeLatN = 60;
EuropeLatS = 35;
EuropeLonW = 345;
EuropeLonE = 30;

% Since the longitude crosses the 360 degrees line, we have to use a | instead of an &
EuropeLat = find(lat > EuropeLatS & lat < EuropeLatN);
EuropeLon = find(lon > EuropeLonW | lon < EuropeLonE);
numPointsEU = length(EuropeLat) * length(EuropeLon);

figure(1);
clf;
%figuresize(340, 125, 'points');
figuresize(340, 170, 'points');
SpaceIndependentComparison(EuropeLat, EuropeLon, numYears, 'Europe', showNumPoints); drawnow;
ax = gca;
ax.YLim = [-0.45 0.1];
ax.XTick = 1:2:7;
ax.XTickLabel = {'1850','1900','1950','2000'};
ax.XTickLabelRotation = 0;
ax.LineWidth = 1.5;
ax.YGrid = 'on';
ax.YTick = -0.4:0.2:0;
ax.XLim = [1 7];
legend({'Model','Measurements'},'Location','southoutside','Orientation','horizontal','box','off');
%legend('off')
ax.FontSize = 12;
title('');



%% Northern Hemisphere

LatN = 90;
LatS = 0;

NHemisphLat = find(lat > LatS & lat < LatN);
NHemisphLon = 1:length(lon);
numPointsNHemisph = length(NHemisphLat) * length(NHemisphLon);

figure(2);
clf;
figuresize(340, 170, 'points');
SpaceIndependentComparison(NHemisphLat, NHemisphLon, numYears, 'Northern Hemisphere', showNumPoints); drawnow;
ax = gca;
ax.YLim = [-0.45 0.1];
ax.XTick = 1:2:7;
ax.XTickLabel = {'1850','1900','1950','2000'};
ax.XTickLabelRotation = 0;
ax.LineWidth = 1.5;
ax.YGrid = 'on';
ax.YTick = -0.4:0.2:0;
ax.XLim = [1 7];
legend({'Model','Measurements'},'Location','southoutside','Orientation','horizontal','box','off');
ax.FontSize = 12;
title('');


