%% Plot a PC for the presentation

dataFolder = '/home/kajivarvdw/Data/';
inputfile = [dataFolder 'EOFsPCs/EOFsPCs_5yearAverage_north.mat'];
load(inputfile);

x=(1850+nYearAverage:nYearAverage:1999) - nYearAverage/2;

centeringMatrix = eye(n) - ones(n, n) ./ n;
%matrixModel = reshape(dataModel, [size(matrixData, 1) n])';
%matrixData = matrixMeasurement;

anomalyFieldModel = centeringMatrix * matrixModel;

projectedPCs = anomalyFieldModel * dataEOFs;

figure(1);
clf;
figuresize(380, 260, 'points');
set(gca,'LineWidth',1.45)
set(gca,'FontSize',16)
ylabel('PC 1');
hold on; 
tmp1 = projectedPCs(:,1)';
p1 = plot(x,tmp1,'Color',[0.368417, 0.506779, 0.709798],'LineWidth',1.8);
set(gcf, 'Color', [250/255, 250/255, 250/255]);

%plot([1850 1999],[0 0],'Color',[0.7 0.7 0.7],'LineWidth',1.7);
set(gca,'LineWidth',1.45)
set(gca,'FontSize',16)
hold on; 
ylabel('PC 1');
xlabel('Year');
tmp2 = dataPCs(:,1)';
p2 = plot(x,tmp2,'Color',[0.922526, 0.385626, 0.209179],'LineWidth',1.8);
set(gcf, 'Color', [250/255, 250/255, 250/255]);
plot([1850 1999],[0 0],'Color',[0.7 0.7 0.7],'LineWidth',1.7);

set(gca,'YLim',[1.1*min([tmp1 tmp2]) 1.1*max([tmp1 tmp2])]);


legend({'Model','Measurements'},'Location','southoutside','Orientation','horizontal','box','off');

%set(gca,'YTick',[-4 0 4]);