

corrCoeff = [0.026, 0.412, 0.549, 0.582, 0.562, 0.712];

figure(1);
figuresize(340, 190, 'points');
clf; hold on;
plot(corrCoeff, '.-', 'Color', [0.560181, 0.691569, 0.194885], 'MarkerSize', 25,'LineWidth',1.5);
xlim([0.8, 6.2]);
ylim([-0.1, 1]);
ax = gca;
ax.XTick = 1:6;
ax.XTickLabel = {'0','1','2','5','10','20'};
ax.YTick = 0:.2:1;
xlabel('Number of years average');
ylabel('Corr. coeff.');
ax.YGrid = 'on';
ax.FontSize = 12;



%%

corrCoeffAssimSigmas = flip([2 1 0.5 0.25 0.1 0.05 0.01]);
%corrCoeffAssim0yearAvg  = flip([0.2823    0.2824    0.2823    0.2814    0.2738    0.2532    0.0934]);
corrCoeffAssim1yearAvg  = flip([0.6748    0.6771    0.6825    0.6889    0.6782    0.6344    0.4652]);
corrCoeffAssim2yearAvg  = flip([0.7440    0.7468    0.7534    0.7620    0.7583    0.7286    0.5960]);
corrCoeffAssim5yearAvg  = flip([0.7519    0.7551    0.7628    0.7761    0.7810    0.7536    0.6292]);
corrCoeffAssim10yearAvg = flip([0.7905    0.7927    0.7993    0.8117    0.8093    0.7693    0.6183]);
corrCoeffAssim20yearAvg = flip([0.8547    0.8563    0.8602    0.8664    0.8612    0.8397    0.7496]);

figure(9);
figuresize(400, 245, 'points');
clf; hold on;
plot(corrCoeffAssim20yearAvg,'LineWidth',1.5);
plot(corrCoeffAssim10yearAvg,'LineWidth',1.5);
plot(corrCoeffAssim5yearAvg,'LineWidth',1.5);
plot(corrCoeffAssim2yearAvg,'LineWidth',1.5);
plot(corrCoeffAssim1yearAvg,'LineWidth',1.5);
%plot(corrCoeffAssim0yearAvg);
colorLines = colormap('lines');
[max20v, max20s] = max(corrCoeffAssim20yearAvg); plot(max20s, max20v, '.', 'Color', colorLines(1,:), 'MarkerSize', 25);
[max10v, max10s] = max(corrCoeffAssim10yearAvg); plot(max10s, max10v, '.', 'Color', colorLines(2,:), 'MarkerSize', 25);
[max5v, max5s] = max(corrCoeffAssim5yearAvg); plot(max5s, max5v, '.', 'Color', colorLines(3,:), 'MarkerSize', 25);
[max2v, max2s] = max(corrCoeffAssim2yearAvg); plot(max2s, max2v, '.', 'Color', colorLines(4,:), 'MarkerSize', 25);
[max1v, max1s] = max(corrCoeffAssim1yearAvg); plot(max1s, max1v, '.', 'Color', colorLines(5,:), 'MarkerSize', 25);
%[max0v, max0s] = max(corrCoeffAssim0yearAvg); plot(max0s, max0v, '.', 'Color', colorLines(6,:), 'MarkerSize', 20);
ylim([0.4, 1]);
xlim([0.5 7.5]);
ax = gca;
ax.XTick = 1:7;
ax.XTickLabel = {'0.01','0.05','0.1','0.25','0.5','1','2'};
ax.YTick = 0:.1:1;
xlabel('\sigma_m');
ylabel('Corr. coeff.');
ax.YGrid = 'on';
ax.FontSize = 13;
legend({'20 year', '10 year', '5 year', '2 year', '1 year'},'Location','eastoutside')