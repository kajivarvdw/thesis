

sigmas = 0.5:0.5:3;
temperatures = sigmas.^2 ./ (sigmas.^2+1) * 21 + (1-sigmas.^2 ./ (sigmas.^2+1))*19;

figure(1);
figuresize(340, 190, 'points');
clf; hold on;
plot(sigmas,temperatures, '.-', 'Color', [0.368417, 0.506779, 0.709798], 'MarkerSize', 25,'LineWidth',1.5);
plot([0 3.5],[21 21], 'Color', [0.6 0.6 0.6], 'LineWidth', 1.5);
plot([0 3.5],[19 19], 'Color', [0.6 0.6 0.6], 'LineWidth', 1.5);
plot([0 3.5],[20.2 20.2], 'Color', [0.560181, 0.691569, 0.194885], 'LineWidth', 1.5);
plot([1.23 1.23],[18 22], 'Color', [0.560181, 0.691569, 0.194885], 'LineWidth', 1.5);
text(3.2,21,'\leftarrow T_o','FontSize',12);
text(3.2,19,'\leftarrow T_m','FontSize',12);
text(3.2,20.2,'\leftarrow T_r','FontSize',12);
leg = legend({'Best estimate'},'Position',[0.5243 0.3684 0.3397 0.0882]);
xlim([0.4, 3.1]);
ylim([18.5, 21.5]);
ax = gca;
ax.YTick = 19:21;
xlabel('\sigma_m');
ylabel('Temperature (°C)');
ax.YGrid = 'on';
ax.FontSize = 12;
set(gcf,'Units','normal')
set(gca,'Position',[0.16 0.21 0.71 0.72])