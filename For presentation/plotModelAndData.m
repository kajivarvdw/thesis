%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Plot the latitudes from 30 deg to 70 deg
% chosen for the data assimilation on EOF's
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dataFolder = '/home/kajivarvdw/Data/';

load([dataFolder 'data.mat']);
load('custmap.mat');

L2 = load('coast');
latCoast = L2.lat;
lonCoast = L2.long;


figure(1);
clf;

%axesm('eckert4', 'Frame', 'on', 'Grid', 'on');
axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [90 0 0], 'MapLatLimit', [10 90]);
hold on;
set(gcf, 'Color', [250/255 250/255 250/255]);
set(gca, 'Box', 'off');
axis off;

[latM, lonM] = meshgrid(lat, lon);

geoshow(latCoast, lonCoast, 'Color', [0 0 0],'LineWidth',2.5);
geoshow(latM(:), lonM(:), 'DisplayType', 'Point', 'Color', 'black', 'Marker', '.', 'MarkerSize', 20);
%custmap = [[1 1 1]; [1 0.65 0]];
%colormap(custmap);
setm(gca, 'GColor', [0.3 0.3 0.3]);
%geoshow('landareas.shp', 'EdgeAlpha', 0, 'FaceColor', [0 0 0], 'FaceAlpha', 0.3);


%%

figure(2);
clf;

%axesm('eckert4', 'Frame', 'on', 'Grid', 'on');
axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [90 0 0], 'MapLatLimit', [10 90]);
hold on;
latInd = 22:29;
latNew = lat(latInd);
set(gcf, 'Color', [250/255 250/255 250/255]);
set(gca, 'Box', 'off');
axis off;

coverage = sum(~isnan(dataMeasurement),3)'/1788;

geoshow(coverage,[1/5.6250 90 0], 'DisplayType', 'texturemap');
%custmap = [[1 1 1]; [1 0.65 0]];
colormap(flip(custmap));
setm(gca, 'GColor', [0.3 0.3 0.3]);
%geoshow('landareas.shp', 'EdgeAlpha', 0, 'FaceColor', [0 0 0], 'FaceAlpha', 0.3);
geoshow(latCoast, lonCoast, 'Color', [0 0 0],'LineWidth',2.5);
cb = colorbar('southoutside','Position',[0.2304    0.1023    0.5768    0.0381]);
ax = gca;
ax.Position(2) = 0.17;
cb.FontSize = 12;
cb.Ticks = 0:0.25:1;
cb.TickLabels = {'0%','25%','50%','75%','100%'};


%%

figure(3);

figuresize(260, 130, 'points');
coverageTime = sum(sum(~isnan(dataMeasurement)))/(2048);
x=1:length(time);
plot(x,coverageTime(:), 'Color', [236,129,27]/255,'LineWidth',1.8);
set(gcf, 'Color', [250/255, 250/255, 250/255]);
ax = gca;
set(gca,'xTick',1:600:1801)
set(gca,'xTickLabel',{'1850', '1900', '1950', '2000'})
ax.YTick = 0:0.25:1;
ax.YTickLabel = {'0%','25%','50%','75%','100%'};
set(gca,'LineWidth',1.5);
set(gca,'YGrid', 'on');
set(gca,'FontSize', 12);
axis([1 1801 0 1]);

