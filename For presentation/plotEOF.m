%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Accuracy analysis of LOVECLIM climate model in comparison to HadCRUT measurements
%
% Bachelor thesis Kaj-Ivar van der Wijst
% Under supervision of Svetlana Dubinkina, CWI, Amsterdam
% November 2015
%
%
% Plot EOF decomposition
% -----------------------------
%
% - Plot Model EOFs, either on world map or stereographic projection
% - Plot Data EOFs
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plotEOF (filename)

dataFolder = '/home/kajivarvdw/Data/';


%% Options

if (nargin == 0)
    filename = 'EOFsPCs_5yearAverage_north.mat';
end

inputfile = [dataFolder 'EOFsPCs/' filename];

assimilatedModel = false;
preAveraged = false;
beginFigID = 17;
plotHadCRUT = true;
plotModel = true;
saveFigures = false;



%% Import data

L = load(inputfile);

% Load variables from file
hemisphere = L.hemisphere;
DINEOFvalidationMode = L.DINEOFvalidationMode;
nYearAverage = L.nYearAverage;
%useSVD = L.useSVD;
weightedLatitude = L.weightedLatitude;

%thresholdDINEOF = L.thresholdDINEOF;
%rMax = L.rMax;
%Noptimal = L.Noptimal;

lat = L.lat;
lon = L.lon;
n = L.n;        p = L.p;


if (isfield(L,'ushcneca'))
    ushcneca = L.ushcneca;
else
    ushcneca = false;
end

dataEOFs = L.dataEOFs;                                      modelEOFs = L.modelEOFs;
dataPCs = L.dataPCs;                                        modelPCs = L.modelPCs;
dataVariancePercentages = L.dataVariancePercentages;        modelVariancePercentages = L.modelVariancePercentages;
%matrixMeasurement = L.matrixMeasurement;
%analyzedField = L.analyzedField;



%% Plot

latOrig = lat;
lonOrig = lon;
[lonMesh, latMesh] = meshgrid(lonOrig, latOrig);
lonMesh(:,end) = 360;
numLat = length(latOrig);

L2 = load('coast');
lat = L2.lat;
long = L2.long;


% Define various extra texts
textForWeighted = '';
if (~weightedLatitude)
    textForWeighted = ', (not weighted)';
end
textForAverage = '';
if (nYearAverage > 0)
    textForAverage = ['Average over: ' num2str(nYearAverage) ' years'];
    if (preAveraged)
        textForAverage = [textForAverage ' (averaged before D.A.)'];
    end
end
nameData = 'HadCRUT data';
if (DINEOFvalidationMode)
    nameData = 'model (reconstructed)';
end
if (ushcneca)
    nameData = 'USHCN ECA data';
end
nameModel = 'model';
if (assimilatedModel)
    nameModel = 'assimilated model';
    if (ushcneca)
        nameModel = 'assimilated (USHCN ECA) model';
    end
end
preAveragedTxt = '';
if (preAveraged)
    preAveragedTxt = '_preaveraged';
end

%% Plot first 4 EOFs on a 2x2 graphics grid

% figure(1);
% plot2x2grid(modelEOFs, modelVariancePercentages, nameModel);
% figure(2);
% plot2x2grid(dataEOFs, dataVariancePercentages, nameData);
% 
% function plot2x2grid (EOFs, variance, text)
%     for q = 1:4
%         subplot(2,2,q);
% %         if (DINEOFvalidationMode && i <= 2)
% %             flipped = 1;
% %         else
% %             flipped = 1;
% %         end
%         geoshow(latMesh, lonMesh, reshape(EOFs(:,q), [64 numLat])',  'DisplayType', 'texturemap');
%         geoshow(lat, long);
%         title(['EOF ' num2str(q) ' of ' text ' (' num2str(variance(q)*100) '% of var.)' textForWeighted textForAverage]);
%         caxis([-0.1 0.1]);
%     end
% end


%% Plot EOF's 1 and 2 on world map or stereographic projection

if (plotModel)
    plotContours (modelEOFs, modelVariancePercentages, nameModel, beginFigID);
end
if (plotHadCRUT)
    plotContours (dataEOFs, dataVariancePercentages, nameData, beginFigID + 2);
end

function plotContours (data, variance, text, figID)
    
    lonNew = [lonOrig(33:end)' - 360 lonOrig(1:32)']';
    % S = shaperead('landareas.shp');
    %eofnum = [[1, 1]; [2, 1]; [3, 1]; [4, 1]; [5, 1]];
    eofnum = [[1, 1]; [2,1]];
    for q = 1:length(eofnum)
        figure(figID + q);
        clf;
        if (strcmp(hemisphere, 'north'))
            axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [90 0 0], 'MapLatLimit', [22 90]);
        elseif (strcmp(hemisphere, 'south'))
            axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [-90 0 0], 'MapLatLimit', [-90 -10]);
        else
            axesm eckert4;
            framem; gridm; axis off;
        end
        % Change latitude to -180 : 180
        tmp = -1*reshape(data(:,eofnum(q,1)),[64 numLat])';
        tmp = [tmp(:,33:end) tmp(:,1:32)];
        lonNewTmp = lonNew;
        lonNewTmp(end) = 180; 
        hold on;
        %geoshow(S, 'EdgeAlpha', 0, 'FaceColor', [.7 .7 .7]);
        if ( eofnum(q, 2) == 1)
            setm(gca, 'GColor', [0.9 0.9 0.9]); 
            contourfm(double(latOrig), double(lonNewTmp), tmp, 12, 'LineStyle', 'none');
            geoshow(lat, long, 'Color', [1 1 1],'LineWidth',1.1);
        else
            setm(gca, 'GColor', [0.3 0.3 0.3]);
            geoshow('landareas.shp', 'EdgeAlpha', 0, 'FaceColor', [.7 .7 .7]);
            contourm(double(latOrig), double(lonNewTmp), tmp, 12, 'LineWidth', 1.5);
        end
        caxis([-0.65*max(abs(tmp(:))), 0.65*max(abs(tmp(:)))]);
        %geoshow(lat, long, 'Color', [1 1 1 .5]);
        %geoshow('landareas.shp', 'EdgeColor', [1 1 1], 'EdgeAlpha', .5, 'FaceColor', [1 1 1], 'FaceAlpha', 0);
        %geoshow('landareas.shp', 'EdgeAlpha', 0, 'FaceColor', [.8 .8 .8])
        %contourm(double(latOrig), double(lonNewTmp), tmp, 12, 'LineWidth', 1.5);
        axis off;
        set(gcf, 'Color', [250/255, 250/255, 250/255]);
        titlestr = sprintf('EOF %d of %s (%2.2f%% of var.)\n%s %s', eofnum(q,1), text, variance(eofnum(q,1))*100, textForAverage, textForWeighted);
        %title(titlestr);
        drawnow;
        imgname = sprintf('EOF%d_%dyearAverage%s_%s_%s.png',eofnum(q,1),nYearAverage,preAveragedTxt,strrep(text,' ','_'),hemisphere{1});
        if (saveFigures)
            export_fig(imgname, '-m2.5');
        end
    end
    
end

end
