%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Plot any quantity on a map projected from
% above. For latitudes from 30 deg to 70 deg
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function tmpPlotSubDomain (data, id, colormapname)

dataFolder = '/home/kajivarvdw/Data/';

extraData = load([dataFolder 'data.mat']);

filled = false;

% L2 = load('coast');
% latCoast = L2.lat;
% lonCoast = L2.long;

coast = load('coast');

lat = extraData.lat;
lon = extraData.lon;

latInd = 22:29;
latNew = lat(latInd);
lonNew = lon;
lonNew(end) = 360;

%figure(id);
%clf;

axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [90 0 0], 'MapLatLimit', [11 90]);
hold on;
set(gcf, 'Color', 'w');
set(gca, 'Box', 'off');
axis off;

tmp = nan(64,32);
tmp(:,latInd) = data;
tmp(tmp > 0) = 0.1*max(tmp(:)) + tmp(tmp > 0);
%geoshow(tmp',[1/5.6250 90 0], 'DisplayType', 'texturemap', 'FaceAlpha', 0.8);


if (filled)
    contourfm(double(latNew), double(lonNew), data', 12, 'LineStyle', 'none')
    geoshow(coast.lat, coast.long, 'Color', 'black')
else
    geoshow('landareas.shp', 'EdgeAlpha', 0, 'FaceColor', [.75 .75 .75]);
    contourm(double(latNew), double(lonNew), data', 12, 'LineWidth', 2);
end

setm(gca, 'GColor', [0.3 0.3 0.3]);
colormap(colormapname);
cmap = colormap;
%cmap(end,:) = [1 1 1];
colormap(flip(cmap))
%geoshow('landareas.shp', 'EdgeAlpha', 0, 'FaceColor', [0 0 0], 'FaceAlpha', 0.3);


drawnow;


end