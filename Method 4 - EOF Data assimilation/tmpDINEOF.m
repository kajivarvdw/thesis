function [eofs, pcs, variance, analyzedMatrix, eigenvals] = tmpDINEOF (matrix, missingValues, clock, scaledEOFs)

if (nargin == 3)
    scaledEOFs = false;
end

% DINEOF:

analyzedMatrix = matrix;
analyzedMatrix(missingValues) = 0;

n = size(matrix, 1);

rMax = 30;
thresholdDINEOF = 0.02;
N = 30;

if (n <= N)
    N = round(3* n / 4);
end

%centeringMatrix = eye(n) - ones(n, n) ./ n;                  % Called H
%analyzedMatrix = centeringMatrix * analyzedMatrix;                % Called X

for r = 1:rMax
    logMsg(['Starting round ' num2str(r) ' of DINEOF process...'], clock);

    % Apply SVD on X_0 or X_a
    prev = analyzedMatrix;                                   % Save current state for error comparison
    [dataA, dataGamma, dataU] = svd(analyzedMatrix);            % Note: Shouldn't we do centring first?
    
    % Truncate EOF series: U_n * D_n * (V_n)'
    truncated = dataA(:,1:N) * dataGamma(1:N,1:N) * (dataU(:,1:N)');

    % Replace missing values in original data by values just calculated with truncated EOFs
    analyzedMatrix(missingValues) = truncated(missingValues);
    
    % Check if convergence is reached (relative difference below threshold percentage)
    diff = abs(analyzedMatrix - prev);
    total = abs(analyzedMatrix);
    errorDINEOF(r) = sum(diff(:)) / sum(total(:));
    logMsg(['Current rel. diff.: ' num2str(errorDINEOF(r))], clock);
    if (errorDINEOF(r) < thresholdDINEOF)
        break;
    end
end

eofs = dataU;
%if (length(dataA) < size(dataGamma, 2))
%    dataGamma = diag(diag(dataGamma));
%end
pcs = dataA * dataGamma;
variance = diag(dataGamma).^2 / sum(diag(dataGamma).^2);
eigenvals = dataGamma;

if (scaledEOFs)
    eofs = dataU;
    pcs = dataA;
end

%fprintf('Eigenvalue 1: %f, eigenvalue 2: %f\n', dataGamma(1,1),dataGamma(2,2));
