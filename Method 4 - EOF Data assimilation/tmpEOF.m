function [eofs, pcs, variance, eigenvals] = tmpEOF (matrix, scaledEOFs)

if (nargin == 1)
    scaledEOFs = false;
end

n = size(matrix, 1);
centeringMatrix = eye(n) - ones(n, n) ./ n;                  % Called H
anomalyField = centeringMatrix * matrix;                % Called X

[A, gamma, U] = svd(anomalyField);

eofs = U;
%if (length(A) < size(gamma, 2))
%    gamma = diag(diag(gamma));
%end
pcs = A * gamma;
eigenvals = gamma;

if (scaledEOFs)
    eofs = U;
    pcs = A;
end

variance = diag(gamma).^2 / sum(diag(gamma).^2);

end