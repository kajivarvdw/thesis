%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Accuracy analysis of LOVECLIM climate model in comparison to HadCRUT measurements
%
% Bachelor thesis Kaj-Ivar van der Wijst
% Under supervision of Svetlana Dubinkina, CWI, Amsterdam
% April 2016
%
% Data Assimilation on EOF level
% ------------------------------
%
% - Import
%       - model
%       - USHCN / ECA datasets
%       - Limit these imports to Northern hemisphere
% - Choose a subset of values on which EOF's are calculated
% - Perform EOF analysis
%       - on model
%       - on USHCN / ECA datasets (DINEOF)
% - Perform data assimilation of EOF1 of data on EOF 1 of model
%       - create covariance matrix of EOF
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


nYearAverage = 5;

showPlots = true;  

% Choose an error for the model (THIS IS OUR PARAMETER) (in degrees)
modelError = 0.2;
rho = 0;

numEOFs = 1;


%% Import data

weightedLatitude = true;
saveOutput = false;
extraLog = false;

dataFolder = GetDataFolder();

clock = tic;

logMsg('Importing model...', clock);
load([dataFolder 'data.mat']);
logMsg('Importing USHCN/ECA datasets...', clock);
load([dataFolder 'data_eca_ushcn_combined.mat']);


n = size(dataModel, 3);
p = size(dataModel, 1) * size(dataModel, 2);

% dataModel = dataModel(:,17:end,:);
% dataObservations = dataObservations(:,17:end,:);
% dataDailyMeas = dataDailyMeas(:,17:end,:);
% lat = lat(17:end);

p = length(lat)*length(lon);
numLat = length(lat);

% Multiply by square root of cosine of latitude (as done in Hannachi2006)
weightedModel = dataModel;


if (weightedLatitude)
    weight = sqrt(cos(lat*pi/180));
    dataObservationsTmp = dataObservations;
    dataObservationsTmp(isnan(dataObservations)) = 0;
    dataMeasurementTmp = dataMeasurement;
    dataMeasurementTmp(isnan(dataMeasurement)) = 0;
    for i = 1:size(dataModel, 3)
        weightedModel(:,:,i) = weightedModel(:,:,i) * diag(weight);
        dataObservationsTmp(:,:,i) = dataObservationsTmp(:,:,i) * diag(weight);
        dataMeasurementTmp(:,:,i) = dataMeasurementTmp(:,:,i) * diag(weight);
    end
    dataObservationsTmp(isnan(dataObservations)) = nan;
    dataObservations = dataObservationsTmp;
    dataMeasurementTmp(isnan(dataMeasurement)) = nan;
    dataMeasurement = dataMeasurementTmp;
end


%% Choose a subset of values on which EOF's will be calculated

logMsg('Choosing a subset and creating reduced space matrices...', clock);

% We need dataModel, dataObservations, matrixDailyMeas

% Choose latitudes 22 to 29 (30 degrees to 70 degrees)
newLatInd = 22:29;
newLat = lat(newLatInd);
numNewLat = length(newLat);
pNew = length(lon) * length(newLat);
weightedModelSub = weightedModel(:,newLatInd,:);
dataObservationsSub = dataObservations(:,newLatInd,:);
dataDailyMeasSub = numDailyMeasObs(:,newLatInd,:);
dataMeasurementSub = dataMeasurement(:,newLatInd,:);

matrixModelSubOrig = reshape(weightedModelSub, [pNew n])';
matrixObservationsSubOrig = reshape(dataObservationsSub, [pNew n])';
matrixDailyMeasSubOrig = reshape(dataDailyMeasSub, [pNew n])';
matrixHadcrutSubOrig = reshape(dataMeasurementSub, [pNew n])';


% Total number of observations per grid point
totalObs2D = sum(dataDailyMeasSub, 3);
totalObs1D = sum(matrixDailyMeasSubOrig, 1);
%tmpPlotSubDomain(totalObs2D.^0.3,2,'hot');




%% Averaging



if (nYearAverage > 0)
    
    % We ignore every additional months which doesn't fit into the n-year bins

    nMonthAverage = nYearAverage * 12;
    totalBins = int16(floor(n / nMonthAverage));

    averagingMatrix = zeros(totalBins, n);

    for b = 0:totalBins-1
        averagingMatrix(b+1, b*nMonthAverage+1:(b+1)*nMonthAverage) = 1;
    end
    
    % Take average of model
    matrixModelSub = averagingMatrix * matrixModelSubOrig / (nMonthAverage);
    
    % Take average of extra dataset
    matrixObservationsSub = matrixObservationsSubOrig;        matrixObservationsSub(isnan(matrixObservationsSubOrig)) = 0;
    numExistingMonths = averagingMatrix * (~isnan(matrixObservationsSubOrig));
    matrixObservationsSub = (averagingMatrix * matrixObservationsSub) ./ (numExistingMonths);
    % Only keep values with at least two months of data per year
    matrixObservationsSub(numExistingMonths < 2 * nYearAverage) = nan;
    
    % Number of daily measurements are added together
    matrixDailyMeasSub = int32(averagingMatrix * double(matrixDailyMeasSubOrig));
    
    % Take average of HadCRUT
    matrixHadcrutSub = matrixHadcrutSubOrig;        matrixHadcrutSub(isnan(matrixHadcrutSubOrig)) = 0;
    numExistingMonths = averagingMatrix * (~isnan(matrixHadcrutSubOrig));
    matrixHadcrutSub = (averagingMatrix * matrixHadcrutSub) ./ (numExistingMonths);
    % Only keep values with at least two months of data per year
    matrixHadcrutSub(numExistingMonths < 2 * nYearAverage) = nan;
    
    
    n = double(totalBins);
    
    
    
    %dataObservations = reshape(matrixDataSub', [64 32 n]);
    %dataModel = reshape(matrixModelSub', [64 32 n]);
    
    
else
    matrixModelSub = matrixModelSubOrig;
    matrixObservationsSub = matrixObservationsSubOrig;
    matrixDailyMeasSub = matrixDailyMeasSubOrig;
    matrixHadcrutSub = matrixHadcrutSubOrig;
end


%% Perform EOF analysis of model on reduced space and DINEOF of USHCN/ECA and HadCRUT

%%% Perform EOF analysis of model on reduced space

logMsg('Performing EOF analysis on reduced model...', clock);



[modelSubEOFs, modelSubPCs, modelSubVariancePercentages, modelSubEigenvals] = tmpEOF (matrixModelSub, true);
%%% If you want to reconstruct matrixModelSub: test = modelSubPCs(:,1:min(pNew,n)) * modelSubEigenvals(1:min(pNew,n),1:min(pNew,n)) * modelSubEOFsOrig(:,1:min(pNew,n))';
modelSubEOFsOrig = modelSubEOFs;
modelSubEOFs = modelSubEOFs * modelSubEigenvals';

testReconstructNew = modelSubPCs * modelSubEOFs';

if (showPlots)
    figure(1);
    clf;
    figuresize(780, 270, 'points');
    set(gcf, 'Color', 'w');

    ax1 = subplot(1,3,1);
    subEOFmodel1 = reshape(modelSubEOFs(:,1), [64 length(newLat)]);
    maxV = max(abs([min(subEOFmodel1(:)), max(subEOFmodel1(:))]));
    tmpPlotSubDomain(subEOFmodel1,1,'parula');
    caxis(0.8 * [-maxV, maxV]);
    title('$E_{model}$(1)', 'Interpreter', 'LaTeX');
    ax1.FontSize = 12;
    ax1.XLim = [-1.5 1.5];
    ax1.YLim = [-1.5 1.5];
    ax1.Box = 'off';
    ax1.XColor = [1 1 1];
    ax1.YColor = [1 1 1];
end
    

%%% Perform DINEOF analysis of HadCRUT on reduced space
% Note that we don't have to project this one, since the reconstructed assim. field is projected
% onto HadCRUT

logMsg('Performing DINEOF analysis of HadCRUT on reduced space...', clock);

% DINEOF:
[hadcrutSubEOFs, hadcrutSubPCs, hadcrutSubVariancePercentages, hadcrutAnalyzed, hadcrutSubEigenvals] = tmpDINEOF(matrixHadcrutSub, isnan(matrixHadcrutSub), clock, false);

% Note that the tmpDINEOF function does the same as DINEOF, but for simplicity reasons, this uses a
% slightly different format, which made it easier to made a new file out of it.

%%% Perform DINEOF analysis of USHCN/ECA on reduced space

logMsg('Performing DINEOF analysis of USHCN/ECA on reduced space...', clock);

% DINEOF:
[dataSubEOFs, dataSubPCs, dataSubVariancePercentages, dataSubAnalyzed, dataSubEigenvals] = tmpDINEOF(matrixObservationsSub, isnan(matrixObservationsSub), clock, true);
dataSubEOFsOrig = dataSubEOFs;
dataSubEOFs = dataSubAnalyzed' * modelSubPCs;    % Project data onto model EOF;
%dataSubEOFs = dataSubEOFs(:,1:min(pNew,n));

% Plot the first EOF of the ECA/USHCN data

if (showPlots)
    ax2 = subplot(1,3,2);
    tmp = (dataSubEigenvals * dataSubEOFsOrig')';
    subEOForig1 = reshape(tmp(:,1), [64 length(newLat)]);
    maxV = max(abs([min(subEOForig1(:)), max(subEOForig1(:))]));
    tmpPlotSubDomain(subEOForig1,2,'parula');
    caxis(0.5 * [-maxV, maxV]);
    title('$E_{data}$(1)', 'Interpreter', 'LaTeX');
    ax2.FontSize = 12;
    ax2.XLim = [-1.5 1.5];
    ax2.YLim = [-1.5 1.5];
    ax2.Box = 'off';
    ax2.XColor = [1 1 1];
    ax2.YColor = [1 1 1];

    ax3 = subplot(1,3,3);
    subEOFnew1 = reshape(dataSubEOFs(:,1), [64 length(newLat)]);
    maxV = max(abs([min(subEOFnew1(:)), max(subEOFnew1(:))]));
    tmpPlotSubDomain(subEOFnew1,3,'parula');
    caxis(0.5 * [-maxV, maxV]);
    title('$\widehat{E}_{data}(1)$', 'Interpreter', 'LaTeX');
    ax3.FontSize = 12;
    ax3.XLim = [-1.5 1.5];
    ax3.YLim = [-1.5 1.5];
    ax3.Box = 'off';
    ax3.XColor = [1 1 1];
    ax3.YColor = [1 1 1];
    cbar3 = colorbar('Location','southoutside');

    tmpPos1 = ax1.Position;
    tmpPos1(3) = 0.25;
    ax1.Position = tmpPos1;
    tmpPos2 = ax2.Position;
    tmpPos2(3) = 0.25;
    ax2.Position = tmpPos2;
    tmpPos3 = ax3.Position;
    tmpPos3(3) = 0.25;
    ax3.Position = tmpPos3;

    % cbar1.Position = [0.05 0.067 0.23 0.034];
    % cbar2.Position = [0.300 0.067 0.23 0.034];
    cbar3.Position = [0.704 0.067 0.23 0.034];
end
    

%% Calculate the error covariance matrices of the EOFs


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Error cov. matrix of model EOF  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

logMsg('Calculating error covariance matrix of model EOF...', clock);


% First source of error: error from model
sigmaModelFromModel = modelError  * modelSubEigenvals(1,1); 
% Since we use a different definition of the EOF (it is now multiplied by the singular value
% gamma(i), we have to multiply the model error by this factor as well. Note that the variable
% modelSubEigenvals actually contains singular values, not eigenvalues.

% Second source of error: error from EOF, using North et al rule of thumb
nStar = n;
errorInEigenvaluesModel = modelSubVariancePercentages * sqrt(2/nStar);
diffEigenvalues12Model = modelSubVariancePercentages(1) - modelSubVariancePercentages(2);
sigmaModelFromEOF1 = abs(modelSubEOFsOrig(:,2)') * errorInEigenvaluesModel(1) / diffEigenvalues12Model * modelSubEigenvals(1,1);

% Combine two sources of error
sigmaModel = sqrt(sigmaModelFromModel.^2 + sigmaModelFromEOF1.^2);

if (showPlots)
    figure(13); plot(1:pNew, sigmaModelFromEOF1, 1:pNew, ones(1,pNew)*sigmaModelFromModel);
    % title('Deviation (error) in model');
    legend({'From EOF', 'From model'},'Location','best'); xlabel('Position in space'); ylabel('Standard deviation','Interpreter','TeX');
    figuresize(380, 190, 'points');
    axis([0 520 0 5.5])
    set(gca,'FontSize',12);
    figure(20);
    clf;
    plotSubDomainMatrix(reshape(sigmaModel', [64 length(newLat)]),1,'default');
end

% Create covariance matrix
P = CreateCovariance(sigmaModel, rho, pNew);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Error cov. matrix of observations EOF  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Actually, this is only calculated during data assimilation. Here, we calculate the variances

% Variance of EOF observations is a constant divided by number of observations.
numExistingObsOrig = totalObs1D / n;
numExistingObs = numExistingObsOrig;

sigmaConstData = 0.78;
sigmaDataFromMeas = sigmaConstData ./ (sqrt(numExistingObs))  * modelSubEigenvals(1,1);

sigmaDataFromMeas(numExistingObs == 0) = nan;     % These values will be discared anyway when limiting the observation space

% Now we also have to add the uncertainty from the EOF decomposition (North et al.)
nStar = round(mean(sum(matrixDailyMeasSub > 0)));
errorInEigenvalues = dataSubVariancePercentages * sqrt(2/nStar);
diffEigenvalues12 = dataSubVariancePercentages(1) - dataSubVariancePercentages(2);
sigmaDataFromEOF1 = abs(dataSubEOFsOrig(:,2)') * errorInEigenvalues(1) / diffEigenvalues12 * dataSubEigenvals(1,1);

if (showPlots)
    figure(12); clf; hold on;
    plot(sigmaDataFromEOF1); plot(sigmaDataFromMeas); 
    %title('Deviation (error) in data');
    legend({'From EOF','From measurements'},'Location','best'); xlabel('Position in space'); ylabel('Standard deviation','Interpreter','TeX');
    figuresize(380, 190, 'points');
    axis([0 520 0 5.5])
    set(gca,'FontSize',12);
    set(gca,'Box','on');
    %%% Missing values in red line are actually infinity

    figure(21);
    clf;
    plotSubDomainMatrix(reshape((sigmaTmp+sigmaDataFromEOF1)', [64 length(newLat)]),0.5,'default');
end
    

% Combine the two sources of error
sigmaData = sqrt(sigmaDataFromMeas.^2 + sigmaDataFromEOF1.^2);


%% Perform data assimilation on EOFs

% Perform data assimilation for every timestep:
timing = int16(numEOF / 10);

assimilatedSubEOFs = zeros(size(dataSubEOFs));
                                                           
for i = 1:numEOFs
    
    tmpObs = dataSubEOFs(:,i); 
    existingObs = find(totalObs1D > 0);
    
    xObs = tmpObs(existingObs);                 % Observation vector
    xModel = modelSubEOFs(:,i);
    
    N = length(existingObs);
    
    % a. Create observation matrix H 
    
    H = zeros(N, pNew);
    for j = 1:N
        H(j, existingObs(j)) = 1;
    end
    
    % b. Create error cov. matrix R for data
    
    sigmaDataTmp = sigmaData;
    sigmaDataTmp(isnan(sigmaDataTmp)) = 0;      % Could be any value
    R = CreateCovariance(H * sigmaDataTmp', 0, N);
    
    % c. Calculate Kalman filter
    
    K = (P * (H')) / (H * P * H' + R);
    
    % d. Perform actual data assimilation
    
    xAnalyzed = xModel + K * (xObs - H * xModel);
    assimilatedSubEOFs(:,i) = xAnalyzed;
    
    % Log every 10%
    if (mod(i,timing) == 0 && extraLog)
        logMsg(['Data assimilation: finished ' num2str(10*i/timing) '%'],clock)
    end
    
end

%% Plot model EOF, data EOF and assimilated EOF


plotEOFnum = 1;
latInd = 22:29;
coast = load('coast');

minV = 0.6 * min([assimilatedSubEOFs(:,plotEOFnum); modelSubEOFs(:,plotEOFnum); dataSubEOFs(:,plotEOFnum)]);
maxV = 0.25 * max([assimilatedSubEOFs(:,plotEOFnum); modelSubEOFs(:,plotEOFnum); dataSubEOFs(:,plotEOFnum)]);

figure(60);
clf;
figuresize(780, 270, 'points');
set(gcf, 'Color', 'w');

% Plot model EOF
ax1 = subplot(1,3,1);
axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [90 0 0], 'MapLatLimit', [11 90]);
hold on; set(gcf, 'Color', 'w'); set(gca, 'Box', 'off'); axis off;
tmp = nan(64,32);
tmp(:,latInd) = reshape(modelSubEOFs(:,plotEOFnum)', [length(lon) length(newLat)]);
tmp(tmp < 0.95 * minV) = 0.95 * minV;
geoshow(tmp',[1/5.6250 90 0], 'DisplayType', 'texturemap', 'FaceAlpha', 0.8);
geoshow(coast.lat, coast.long, 'Color', 'black')
caxis([minV maxV]);
ax1.FontSize = 12;
ax1.XLim = [-1.2 1.2];
ax1.YLim = [-1.5 1.5];
ax1.Box = 'off';
ax1.XColor = [1 1 1];
ax1.YColor = [1 1 1];
title('Background EOF', 'Interpreter', 'LaTeX');


% Plot data EOF
ax2 = subplot(1,3,2);
axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [90 0 0], 'MapLatLimit', [11 90]);
hold on; set(gcf, 'Color', 'w'); set(gca, 'Box', 'off'); axis off;
tmp = nan(64,32);
tmpSub = reshape(dataSubEOFs(:,plotEOFnum)', [length(lon) length(newLat)]);
tmpSub(totalObs2D == 0) = nan;
tmp(:,latInd) = tmpSub;
tmp(tmp < 0.95 * minV) = 0.95 * minV;
geoshow(tmp',[1/5.6250 90 0], 'DisplayType', 'texturemap', 'FaceAlpha', 0.8);
geoshow(coast.lat, coast.long, 'Color', 'black')
caxis([minV maxV]);
ax2.FontSize = 12;
ax2.XLim = [-1.2 1.2];
ax2.YLim = [-1.5 1.5];
ax2.Box = 'off';
ax2.XColor = [1 1 1];
ax2.YColor = [1 1 1];
title('Observation EOF', 'Interpreter', 'LaTeX');


% Plot assimilated EOF
ax3 = subplot(1,3,3);
axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [90 0 0], 'MapLatLimit', [11 90]);
hold on; set(gcf, 'Color', 'w'); set(gca, 'Box', 'off'); axis off;
tmp = nan(64,32);
tmp(:,latInd) = reshape(assimilatedSubEOFs(:,plotEOFnum)', [length(lon) length(newLat)]);
tmp(tmp < 0.95 * minV) = 0.95 * minV;
geoshow(tmp',[1/5.6250 90 0], 'DisplayType', 'texturemap', 'FaceAlpha', 0.8);
geoshow(coast.lat, coast.long, 'Color', 'black')
caxis([minV maxV]);
ax3.FontSize = 12;
ax3.XLim = [-1.2 1.2];
ax3.YLim = [-1.5 1.5];
ax3.Box = 'off';
ax3.XColor = [1 1 1];
ax3.YColor = [1 1 1];
title('Assimilated EOF', 'Interpreter', 'LaTeX');

colormap('jet'); cmap = flip(colormap); cmap(end,:) = [1 1 1]; colormap(flip(cmap)); drawnow;





%% Reconstruct the assimilated field using the assimilated EOF's



matrixAssimilatedSub = modelSubPCs(:,1:numEOFs) * assimilatedSubEOFs(:,1:numEOFs)'...
    + modelSubPCs(:,(numEOFs+1):end) * modelSubEOFs(:,(numEOFs+1):end)';



%% Project the reconstructed field onto the HadCRUT EOF

centeringMatrix = eye(n) - ones(n, n) ./ n;
%matrixModel = reshape(dataModel, [size(matrixData, 1) n])';
%matrixData = matrixMeasurement;

Xprime = centeringMatrix * matrixAssimilatedSub;

projectedAssimPCs = Xprime * hadcrutSubEOFs;

corrPCmat = corrcoef(projectedAssimPCs(:,1),hadcrutSubPCs(:,1));
corrPC = corrPCmat(1,2);
rmsDiff = rms(projectedAssimPCs(:,1) - hadcrutSubPCs(:,1));
fprintf('\nnYearAverage: %d\n',nYearAverage);
fprintf('Sigma: %f, corr. assim/data: %1.3f, RMS diff: %1.3f\n',modelError, corrPC, rmsDiff);

% Plot projected PC
figure(40); clf;
figuresize(380, 190, 'points');
xlabel('Year');
ylabel('Principal component');
%legendLabels = {'PC 1 projected assim.', 'PC 1 HadCRUT data'};
hold on;
if (nYearAverage > 0)
    x=1850+nYearAverage:nYearAverage:1999;
else
    x=1:length(time);
    set(gca,'xTick',1:300:1801)
    set(gca,'xTickLabel',{'1850', '1875', '1900', '1925', '1950', '1975','2000'})
    axis([1 1801 -inf inf]);
end
plot(x,projectedAssimPCs(:,1));
plot(x,hadcrutSubPCs(:,1));
%legend(legendLabels,'Location','best');
titlestr = sprintf('PC 1 of %d year average\nCorrelation projected/HadCRUT: %1.3f\nAssim. projected on HadCRUT data' ,nYearAverage, corrPC);
%title(titlestr);
set(gcf, 'Color', 'w');
drawnow;



% Project the original model onto the HadCRUT EOF

centeringMatrix = eye(n) - ones(n, n) ./ n;
%matrixModel = reshape(dataModel, [size(matrixData, 1) n])';
%matrixData = matrixMeasurement;

Xprime = centeringMatrix * matrixModelSub;

projectedAssimPCs = Xprime * hadcrutSubEOFs;

corrPCmat = corrcoef(projectedAssimPCs(:,1),hadcrutSubPCs(:,1));
corrPC = corrPCmat(1,2);
rmsDiff = rms(projectedAssimPCs(:,1) - hadcrutSubPCs(:,1));
fprintf('Sigma: %f, corr. orig model/data: %1.3f, RMS diff: %1.3f\n',modelError, corrPC, rmsDiff);

% Plot projected PC
%figure(41); clf;
%figuresize(380, 190, 'points');
%xlabel('Year');
%ylabel('Principal component');
legendLabels = {'PC 1 projected assim.', 'PC 1 HadCRUT data','PC 1 original model'};
%hold on;
% if (nYearAverage > 0)
%     x=1850+nYearAverage:nYearAverage:1999;
% else
%     x=1:length(time);
%     set(gca,'xTick',1:300:1801)
%     set(gca,'xTickLabel',{'1850', '1875', '1900', '1925', '1950', '1975','2000'})
%     axis([1 1801 -inf inf]);
% end
plot(x,projectedAssimPCs(:,1),'--','Color',[0.7 0.7 0.7]);
%plot(x,hadcrutSubPCs(:,i));
legend(legendLabels,'Location','best');
%titlestr = sprintf('PC 1 of %d year average\nCorrelation projected/HadCRUT: %1.3f\nOrig. model projected on HadCRUT data' ,nYearAverage, corrPC);
%title(titlestr);
%set(gcf, 'Color', 'w');
drawnow;



%% Finalize

logMsg('Done.', clock);


