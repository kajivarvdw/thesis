function AverageOverGivenArea (latitudes, longitudes, nyears, name, showNumPoints, dataset)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Accuracy analysis of LOVECLIM climate model in comparison to HadCRUT measurements
%
% Bachelor thesis Kaj-Ivar van der Wijst
% Under supervision of Svetlana Dubinkina, CWI, Amsterdam
% November 2015
%
%
% Space Independent Comparisons
% -----------------------------
%
% - Average over a given region
% - Calculate n-year average
% - Calculate covariance and correlations
%
%
% Input arguments
% ---------------
%
% - latitudes: array of indices of latitudes
% - longitudes: array of indices of longitudes
% - nyears: average over nyears years
% - name: display name on title of plots
% - showNumPoints: if true, shows the number of points used in the averaging for the observations
% - dataset: can be either 'hadcrut' (HadCRUT dataset), or 'eca' (ECA dataset)
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (nargin < 6)
    dataset = 'hadcrut';
end


%% Import data

clock = tic;
logMsg('Importing data', clock);
dataFolder = GetDataFolder();

load([dataFolder 'data.mat']);
if (strcmp(dataset, 'eca'))
    load([dataFolder 'eca/data_eca.mat']);
    dataMeasurement = dataECA;
end

% We now have access to the following variables:
% - dataMeasurement (64x32x1788)
% - dataModel (64x32x1788)
% - lat (32x1)
% - lon (64x1)
% - time (1788x2)

%% Average over the region

logMsg('Calculating average over the region for every time frame', clock);

ChosenLatitudes = latitudes;
ChosenLongitudes = longitudes;
numPointsEU = length(ChosenLatitudes) * length(ChosenLongitudes);

RegionAveragesModel = zeros(1, length(time));
RegionAveragesMeasurement = zeros(1, length(time));
RegionAveragesMeasurementNumPoints = zeros(1, length(time)); % Needed for calculating the error

% For every point in time, calculate average anomaly in Europe, both for model and measurements
for i = 1:length(time)
    RegionAveragesModel(i) = mean( reshape(dataModel(ChosenLongitudes, ChosenLatitudes, i), [1 numPointsEU]) );
    % We have to exclude the missing data for the measurements
    RegionFlattenedMeas = reshape(dataMeasurement(ChosenLongitudes, ChosenLatitudes, i), [1 numPointsEU]);
    RegionAveragesMeasurement(i) = mean(RegionFlattenedMeas(find(~isnan(RegionFlattenedMeas))));
    RegionAveragesMeasurementNumPoints(i) = length(find(~isnan(RegionFlattenedMeas)));
end

clear EuropeFlattenedMeas



%% n-year average

numAverageYears = nyears;

logMsg(['Calculating average over period of ' num2str(numAverageYears) ' years'], clock);

beginYear = time(1,1);
numTimeSpans = floor((time(end,1) - beginYear)/numAverageYears) - 1;

% Allocate space
modelAverageYears = zeros(numTimeSpans, 1);
measurementAverageYears = zeros(numTimeSpans, 1);
measurementNumPoints = zeros(numTimeSpans, 1);
averageYearsTicks = {};

for i = 0:numTimeSpans
    % Check if still enough data available
    if ((i+1)*numAverageYears*12 > length(time))
        break;
    end
    
    startYear = beginYear + numAverageYears * i;
    endYear = beginYear + numAverageYears * (i+1);
    times = find(time(:,1) >= startYear & time(:,1) < endYear);
    
    modelAverageYears(i + 1) = mean( RegionAveragesModel(times) );
    measurementAverageYears(i + 1) = mean( RegionAveragesMeasurement(times) );
    
    measurementNumPoints(i + 1) = sum(RegionAveragesMeasurementNumPoints(times));
    
    averageYearsTicks{i + 1} = strjoin({num2str(startYear) '-' num2str(endYear)});
end



%% Plots


x = 1:length(modelAverageYears);

rho = corrcoef(modelAverageYears, measurementAverageYears);
globError = rms(modelAverageYears - measurementAverageYears);

if (showNumPoints)
    [hAx,hLine1,hLine2] = plotyy([x',x'],[modelAverageYears,measurementAverageYears],x,measurementNumPoints,'plot','stem');
    hLine2.LineStyle = ':';
    
    ylabel(hAx(1), 'Anomalies');
    ylabel(hAx(2), 'Num. of points');
    
    rhoNumPoints = corrcoef(measurementNumPoints, modelAverageYears - measurementAverageYears);
    rhoNumPointsAbs = corrcoef(measurementNumPoints, abs(modelAverageYears - measurementAverageYears));

    titleString = {strjoin({num2str(numAverageYears)  'year average for' name }), ['(ρ=' num2str(rho(2,1)) ', ρ(#/Δ)=' num2str(rhoNumPoints(2,1)) ', ρ(#/abs(Δ))=' num2str(rhoNumPointsAbs(2,1)) ')'], ['global error = ' num2str(globError)]};

    disp(titleString)
    disp(['Correlation between num. of points and diff: ' num2str(rhoNumPoints(2,1))])
    disp(['Correlation between num. of points and abs(diff): ' num2str(rhoNumPointsAbs(2,1))])
else
    plot(x,modelAverageYears,'Color',[0.368417, 0.506779, 0.709798],'LineWidth',1.5);
    hold on; plot(x,measurementAverageYears,'Color',[0.922526, 0.385626, 0.209179],'LineWidth',1.5);
    titleString = {strjoin({num2str(numAverageYears)  'year average for' name }), ['(ρ=' num2str(rho(2,1)) ', glob.error=' num2str(globError) ')']};
    %titleString = {strjoin({num2str(numAverageYears)  'year average for' name })};
end

title(titleString);
ylabel('Anomaly (°C)');
legend('Model','Measurement','Location','southeast');
ax = gca;
tickDiff = max(1,floor(length(averageYearsTicks) / 5));
ax.XTick = 1:tickDiff:length(averageYearsTicks);
ax.XTickLabel = averageYearsTicks(1:tickDiff:length(averageYearsTicks));
ax.XTickLabelRotation = 45;



%% Covariance and correlations

%covarianceAverageYears = cov(modelAverageYears, measurementAverageYears)
%correlationAverageYears = corrcoef(modelAverageYears, measurementAverageYears)
%covarianceEuropeAnnual = cov(EuropeAnnualModel, EuropeAnnualMeasurement)
%correlationEuropeAnnual = corrcoef(EuropeAnnualModel, EuropeAnnualMeasurement)


logMsg('Finished', clock);




