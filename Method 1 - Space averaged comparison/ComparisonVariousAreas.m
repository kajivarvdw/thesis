%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Averaging over various areas
% ----------------------------
%
% - Average over Europe for various year averagings
% - Average over Northern Hemisphere for various year averagings
% - Average over Southern Hemisphere for various year averagings
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


dataFolder = GetDataFolder();

load([dataFolder 'data.mat']);

numYearsLow = 5;        % Average over 5 years
numYearsHigh = 20;      % Average over 20 years
showNumPoints = false;  % Number of existing grid points for observation



%% Europe

% First calculate which points belong to Europe
EuropeLatN = 60;
EuropeLatS = 35;
EuropeLonW = 345;
EuropeLonE = 30;

% Since the longitude crosses the 360 degrees line, we have to use a | instead of an &
EuropeLat = find(lat > EuropeLatS & lat < EuropeLatN);
EuropeLon = find(lon > EuropeLonW | lon < EuropeLonE);
numPointsEU = length(EuropeLat) * length(EuropeLon);

figure(1);
clf;
subplot(2,1,1);
AverageOverGivenArea(EuropeLat, EuropeLon, numYearsLow, 'Europe', showNumPoints); drawnow;
subplot(2,1,2);
AverageOverGivenArea(EuropeLat, EuropeLon, numYearsHigh, 'Europe', showNumPoints); drawnow;



%% Northern Hemisphere

LatN = 90;
LatS = 0;

NHemisphLat = find(lat > LatS & lat < LatN);
NHemisphLon = 1:length(lon);
numPointsNHemisph = length(NHemisphLat) * length(NHemisphLon);

figure(2);
clf;
subplot(2,1,1);
AverageOverGivenArea(NHemisphLat, NHemisphLon, numYearsLow, 'Northern Hemisphere', showNumPoints); drawnow;
subplot(2,1,2);
AverageOverGivenArea(NHemisphLat, NHemisphLon, numYearsHigh, 'Northern Hemisphere', showNumPoints); drawnow;

% LatN = 90;
% LatS = 30;
% 
% NHemisphLat = find(lat > LatS & lat < LatN);
% NHemisphLon = 1:length(lon);
% numPointsNHemisph = length(NHemisphLat) * length(NHemisphLon);
% 
% subplot(2,2,3);
% AverageOverGivenArea(NHemisphLat, NHemisphLon, numYearsLow, 'Northern Hemisphere (no tropics)', showNumPoints); drawnow;
% subplot(2,2,4);
% AverageOverGivenArea(NHemisphLat, NHemisphLon, numYearsHigh, 'Northern Hemisphere (no tropics)', showNumPoints); drawnow;



% Southern Hemisphere

LatN = 0;
LatS = -90;

SHemisphLat = find(lat > LatS & lat < LatN);
SHemisphLon = 1:length(lon);
numPointsSHemisph = length(SHemisphLat) * length(SHemisphLon);

figure(3);
clf;
subplot(2,1,1);
AverageOverGivenArea(SHemisphLat, SHemisphLon, numYearsLow, 'Southern Hemisphere', showNumPoints); drawnow;
subplot(2,1,2);
AverageOverGivenArea(SHemisphLat, SHemisphLon, numYearsHigh, 'Southern Hemisphere', showNumPoints); drawnow;

% 
% LatN = -30;
% LatS = -90;
% 
% SHemisphLat = find(lat > LatS & lat < LatN);
% SHemisphLon = 1:length(lon);
% numPointsSHemisph = length(SHemisphLat) * length(SHemisphLon);
% 
% subplot(2,2,3);
% AverageOverGivenArea(SHemisphLat, SHemisphLon, numYearsLow, 'Southern Hemisphere (no tropics)', showNumPoints); drawnow;
% subplot(2,2,4);
% AverageOverGivenArea(SHemisphLat, SHemisphLon, numYearsHigh, 'Southern Hemisphere (no tropics)', showNumPoints); drawnow;


