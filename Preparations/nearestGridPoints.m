function [ coordinates, points ] = nearestGridPoints( x, y )
%NEARESTGRIDPOINTS Gives the four neighbouring grid points in measurements grid (5 degrees)
%   

a = floor((x-2.5) / 5);
b = floor((y-2.5) / 5);

xMin = a*5 + 2.5;
yMin = b*5 + 2.5;

coordinates = [xMin yMin; xMin+5 yMin; xMin yMin+5; xMin+5 yMin+5];

% 87.5 (17) -> 1, -87.5 (-18) -> 36
j = int16(-b + 18);
numGhostCells = 1;
i = int16(a + 1 + numGhostCells); % + 36 is because of extended longitude
points = [i j; i+1 j; i j-1; i+1 j-1];


end