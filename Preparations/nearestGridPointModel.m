function [ latIndex, lonIndex] = nearestGridPointModel( lat, lon )
%NEARESTGRIDPOINTSMODEL Gives the nearest grid point in the 5.625 degree grid
%   


%% Latitude

allLat =   [-85.7606 -80.2688 -74.7445 -69.2130 -63.6786 -58.1430 -52.6065 -47.0696...
  -41.5325 -35.9951 -30.4576 -24.9199 -19.3822 -13.8445 -8.3067 -2.7689 2.7689 8.3067...
   13.8445  19.3822  24.9199  30.4576  35.9951  41.5325  47.0696  52.6065  58.1430...
   63.6786  69.2130  74.7445  80.2688  85.7606]';

latIndex = length(allLat);
newLat = allLat(end);

for i = 1:length(allLat)-1
    if (lat < allLat(i) + (allLat(i + 1) - allLat(i)) / 2)
        latIndex = i;
        newLat = allLat(i);
        break;
    end
end


%% Longitude

b = round(lon / 5.625);
lonIndex = int32(b + 1);
newLon = b * 5.625;

if (newLon < 0)
    newLon = newLon + 360;
    lonIndex = lonIndex + 64;
end

%newLat2 = {latIndex, newLat};
%newLon2 = {lonIndex, newLon};

end