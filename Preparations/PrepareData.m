%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Preparation of data to match model and measurements
%
% - Uniform longitudes (0 to 360 degrees)
% - Linear interpolation of measurements
% - Correlations between interpolated versions of HadCRUT3 and HadCRUT4
% - Human readable months/years (2D matrix: [year month]) and corresponding times
% - Anomalies for model
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
clock = tic;
outputFile = '../../Data/data_3missingextension.mat';


%% Import data

fileModel = '../../Data/P1_ICLMALL5m3_ts.nc';
%fileModel = '../../Data/UnpertFy850Py1500_1_ts.nc';
fileMeasurement = '../../Data/HadCRUT.4.4.0.0.median.nc';

logMsg('Importing data from NetCDF files', clock);

ncModel = netcdf.open(fileModel);
ncMeasurement = netcdf.open(fileMeasurement);

latMeasurementOrig     = netcdf.getVar(ncMeasurement, 0);
lonMeasurementOrig     = netcdf.getVar(ncMeasurement, 1);
timeMeasurementTmp = netcdf.getVar(ncMeasurement, 2);
dataMeasurementOrig    = netcdf.getVar(ncMeasurement, 3);


latModel     = netcdf.getVar(ncModel, 0);
lonModel     = netcdf.getVar(ncModel, 1);
timeModelTmp = netcdf.getVar(ncModel, 2);
dataModelTmp = netcdf.getVar(ncModel, 3);
startYearModel = 1849;

sizeLon = length(lonModel);
sizeLat = length(latModel);
sizeTimeModel = length(timeModelTmp);
sizeTimeMeasurement = length(timeMeasurementTmp);

%clear ncModel ncMeasurement fileModel fileMeasurement


%% Uniform longitudes (0 to 360 degrees)

logMsg('Creating uniform longitudes', clock);

lonMeasurementOrig(lonMeasurementOrig < 0) = lonMeasurementOrig(lonMeasurementOrig < 0) + 360;
lonMeasurementOrig = [lonMeasurementOrig(37:end); lonMeasurementOrig(1:36)];

for i = 1:sizeTimeMeasurement
    dataMeasurementOrig(:,:,i) = [dataMeasurementOrig(37:end,:,i); dataMeasurementOrig(1:36,:,i)];
end


%% Linear interpolation of measurements

logMsg('Starting linear interpolation of measurements', clock);

dataMeasurementOrig(dataMeasurementOrig < -90) = NaN;

%test = dataMeasurementOrig;
test = netcdf.getVar(netcdf.open('../../Data/HadCRUT3_Prepared_wrt_1979_2007.nc'), 3);
test(test < -90) = NaN;

dataMeasurement = zeros(sizeLon, sizeLat, length(timeMeasurementTmp));
%[gridLatMeas, gridLonMeas] = meshgrid(latMeasurementOrig, lonMeasurementOrig);
%[gridLatModel, gridLonModel] = meshgrid(latModel, lonModel);

% Extend grid to interpolate boundaries correctly
%gridLatModelExt = [gridLatModel(33:end,:); gridLatModel; gridLatModel(1:32,:)];
%gridLonModelExt = [gridLonModel(33:end,:)-360; gridLonModel; gridLonModel(1:32,:)+360];
%
%gridLatMeasExt = [gridLatMeas(37:end,:); gridLatMeas; gridLatMeas(1:36,:)];
%gridLonMeasExt = [gridLonMeas(37:end,:)-360; gridLonMeas; gridLonMeas(1:36,:)+360];

latNew = latModel;
lonNew = [lonModel(end,:)-360; lonModel; lonModel(1,:)+360];
timing = int16(sizeTimeMeasurement / 10);

for i = 1:sizeTimeMeasurement
    dataMeasurementOrigExt = [...
        dataMeasurementOrig(end,:,i);...
        dataMeasurementOrig(:,:,i);...
        dataMeasurementOrig(1,:,i)...
    ];
    % Use Interpolate(_,_,_,true) to use only nearest neighbour interpolation
    dataMeasurementExt = fliplr(Interpolate(dataMeasurementOrigExt, latNew, lonNew));
    
    %dataMeasurementExt = interp2(...
    %    gridLatMeasExt, gridLonMeasExt,...
    %    dataMeasurementOrigExt(:,:), ...
    %    gridLatModelExt, gridLonModelExt,'nearest');
    
    % Only save original part
    dataMeasurement(:,:,i) = dataMeasurementExt(2:end-1,:);
    
    % Log every 10%
    if (mod(i,timing) == 0)
        logMsg(['Interpolation: finished ' num2str(10*i/timing) '%'],clock)
    end
end

% Clear extra parameters
clear gridLatModelExt gridLonModelExt gridLatMeasExt gridLonMeasExt
clear dataMeasurementOrigExt dataMeasurementExt



%% Correlations between HadCRUT3 and HadCRUT4 interpolated values

logMsg('Calculating correlations between HadCRUT3 and HadCRUT4',clock);

correlations3and4 = zeros(min(sizeTimeMeasurement, length(test)),1);

for i = 1:min(sizeTimeMeasurement, length(test))
    % Calculate correlations for all data points where both HadCRUT3 and HadCRUT4 have existing
    % data (!= NaN).
    setHadCRUT3 = test(:,:,i); setHadCRUT3 = setHadCRUT3(:);
    setHadCRUT4 = dataMeasurement(:,:,i); setHadCRUT4 = setHadCRUT4(:);
    existingValues = not(isnan(setHadCRUT3) | isnan(setHadCRUT4));
    setHadCRUT3 = setHadCRUT3(existingValues);
    setHadCRUT4 = setHadCRUT4(existingValues);
    
    correlations3and4(i) = corr(setHadCRUT3, setHadCRUT4);
    
    % Plot the differences
    % imagesc(test(:,:,i) - dataMeasurement(:,:,i), [-10 10]);
    % drawnow;
    
end

clear existingValues setHadCRUT3 setHadCRUT4

%% Human readable months/years (2D matrix: [year month]) and corresponding times

logMsg('Creating human readable timestamps',clock);

% First we have to make the timestamps uniform: make an array with [year, month]
timeModel = zeros(length(timeModelTmp),2);
timeMeasurement = zeros(length(timeMeasurementTmp),2);

for i = 0:length(timeModelTmp)-1
    timeModel(i+1,:) = [startYearModel + floor(i/12); mod(i,12)+1];
end

% Same for the measurements (for the time being, leave it as days after 1850-01-01)
for i = 0:length(timeMeasurementTmp)-1
    timeMeasurement(i+1,:) = [1850 + floor(i/12); mod(i,12)+1];
end

% Only keep corresponding timestamps (1850-01 to 1998-12)
timeModel = timeModel(13:end,:);
dataModelTmp = dataModelTmp(:,:,13:end);

timeMeasurement = timeMeasurement(1:1788,:);
dataMeasurement = dataMeasurement(:,:,1:1788);

clear timeModelTmp timeMeasurementTmp


%% Anomalies for model

logMsg('Calculating anomalies for model',clock)

dataModel = dataModelTmp; % dataModel will contain anomalies

% Anomalies are calculated by taking average of time values between 1961 and 1990

startYearAverage = 1961;
endYearAverage = 1990;

timeIndices = find(timeModel(:,1) >= startYearAverage & timeModel(:,1) <= endYearAverage);
modelChosenYearsData = dataModelTmp(:,:,timeIndices);
modelChosenYearsTime = timeModel(timeIndices,:);

averages = zeros(length(lonModel),length(latModel),12);

% Calculate average for every month
for month = 1:12
    % Select values for this month
    indicesMonth = modelChosenYearsTime(:,2) == month;
    % Calculate average per data point
    averages(:,:,month) = mean(modelChosenYearsData(:,:,indicesMonth),3);
end

save('averagesRefAnomalies.mat','averages');

% Remove averages from whole dataset, per month
for i = 1:length(dataModelTmp)
    dataModel(:,:,i) = dataModelTmp(:,:,i) - averages(:,:, timeModel(i,2));
end

clear timeIndices modelChosenYearsData modelChosenYearsTime indicesMonth


%% Save variables to file

logMsg(['Saving file to ' outputFile],clock);

% Rename lat, lon and time (since they are equal for the measurements and the model)
lat = latModel;
lon = lonModel;
time = timeModel;

save(outputFile, 'lat', 'lon', 'time', 'dataModel', 'dataMeasurement', 'correlations3and4');



%% Plot

logMsg('Plotting data',clock);

%dataMeasurement(dataMeasurement < -9999) = -200;
%dataModelTmp(dataModelTmp < 9999) = -40;
load coast;

figure(1);
for i = 1:20:length(timeModel)
    subplot(3,1,1);
    imagesc(test(:,:,i)', [-10 10]);
    title('Original HadCRUT3');
    subplot(3,1,2);
    imagesc(dataMeasurement(:,:,i)', [-10, 10]);
    title('Interpolated HadCRUT4');
    subplot(3,1,3);
    imagesc(dataModel(:,:,i)', [-10 10]);
    title('LOVECLIM model')
    drawnow;
end

% clf;
% axesm eckert4;
% framem; gridm; axis off;
% for i = 1:50:length(timeModel)
%     geoshow(dataModel(:,:,i)',[1/5.6250 90 0], 'DisplayType', 'texturemap');
%     geoshow(lat, long);
%     drawnow;
% end



logMsg('Finished',clock);











 











%% Linear interpolation of measurements (method 2: griddata() )
% 
% dataMeasurementsMeth2 = zeros(sizeLon, sizeLat, length(timeMeasurementTmp));
% [gridLatMeas, gridLonMeas] = meshgrid(latMeasurementOrig, lonMeasurementOrig);
% latMeasFlattened = double(gridLatMeas(:)); % Cast to double since griddata() needs doubles
% lonMeasFlattened = double(gridLonMeas(:));
% 
% [gridLatModel, gridLonModel] = meshgrid(double(latModel), double(lonModel));
% 
% for i=1:length(timeMeasurementTmp)
%     dataMeasurementsFlattened = dataMeasurementOrig(:,:,i);
%     dataMeasurementsFlattened = double(dataMeasurementsFlattened(:));
%     % Remove all coordinates and data points from missing data
%     latMeasFlattenedTmp = latMeasFlattened(dataMeasurementsFlattened > -80);
%     lonMeasFlattenedTmp = lonMeasFlattened(dataMeasurementsFlattened > -80);
%     dataMeasurementsFlattened = dataMeasurementsFlattened(dataMeasurementsFlattened > -80);
%     
%     dataMeasurementsMeth2(:,:,i) = griddata(...
%         latMeasFlattenedTmp, lonMeasFlattenedTmp, dataMeasurementsFlattened,...
%         gridLatModel, gridLonModel);
%     
%     % Remove all data points which have at least two neighbouring original points missing
%   
%     
%     if i == 1000
%         figure(4);
%         imagesc(dataMeasurementsMeth2(:,:,i),[-10 10]);
%         figure(5);
%         imagesc(dataMeasurementOrig(:,:,i),[-10 10]);
%     end
%    
% end