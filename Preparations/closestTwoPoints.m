function [closestTwo, closestCoordinates] = closestTwoPoints (point, points, coordinates)
distances = zeros(4,1);
for i = 1:4
    distances(i) = norm(coordinates(i,:) - point);
end
[~,ind] = sort(distances);
closestTwo = [points(ind(1),:); points(ind(2),:)];
closestCoordinates = [coordinates(ind(1),:); coordinates(ind(2),:)];

end