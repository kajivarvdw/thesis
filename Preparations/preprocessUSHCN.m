%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Preprocessing of US HCN dataset
%
% - Import metadata about stations
% - Import and combine all station data
% - Average to 5 deg x 5 deg grid, and calculate variance
% - Calculate anomalies
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

process_stations = false;

stations_file = '../../Data/ushcn/station.mat';
statereference_output_file = '../../Data/ushcn/state_ref_ushcn.mat';
USHCN_output_file = '../../Data/ushcn/data_ushcn.mat';

tic;

%% Import metadata about stations

if (process_stations)
    
    filename = '../../Data/ushcn/raw/ushcn-stations.txt';
    formatSpec = '%d%f%f%f%s%*s%*s%*s%*s%*s%*s%*s%*s%[^\n\r]';
    delimiter = ' ';
    fileID = fopen(filename,'r');
    dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'MultipleDelimsAsOne', true,  'ReturnOnError', false);
    fclose(fileID);
    
    station_ids = dataArray{:, 1};
    station_lat = dataArray{:, 2};
    station_lon = dataArray{:, 3};
    station_state = dataArray{:, 5};
    
    N = length(station_ids);
    
    % Save useful parts
    save(stations_file, 'N', 'station_ids', 'station_lat', 'station_lon', 'station_state');

else
    load(stations_file);
end

% Create a hash table for the station_ids, which is much faster than using 'find'
station_ids_inv = zeros(max(station_ids), 1, 'int32');
for i = 1:length(station_ids)
    station_ids_inv(station_ids(i)) = i;
end

%% Import data from text file.

filename = '../../Data/ushcn/raw/ushcn2014_FLs_52i_tavg.txt';
formatSpec = '%3s%8d%5d%6f%c%c%c%6f%c%c%c%6f%c%c%c%6f%c%c%c%6f%c%c%c%6f%c%c%c%6f%c%c%c%6f%c%c%c%6f%c%c%c%6f%c%c%c%6f%c%c%c%6f%c%c%c%6f%c%c%c%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', '', 'WhiteSpace', '',  'ReturnOnError', false);
fclose(fileID);


%% Save relevant parts

data_station_ids = dataArray{2};
data_years = dataArray{3};
data_temp = zeros(length(dataArray{3}), 12);
data_dm = zeros(length(dataArray{3}), 12);
data_temp(:,1) = FahrenheitToCelsius(dataArray{4}/10);      % In tenth of degrees Fahrenheit
data_dm(:,1) = USHCNdm(dataArray{5});        % Data measurement: A - H or a - i(q): number of days
                                % missing (A/a = 1, B/b = 2, ...) for calculation of monthly average
                                % E/I(Q)/.: missing value, should be nan
data_temp(:,2) = FahrenheitToCelsius(dataArray{8}/10);
data_dm(:,2) = USHCNdm(dataArray{9});
data_temp(:,3) = FahrenheitToCelsius(dataArray{12}/10);     % In tenth of degrees Fahrenheit
data_dm(:,3) = USHCNdm(dataArray{13});
data_temp(:,4) = FahrenheitToCelsius(dataArray{16}/10);     % In tenth of degrees Fahrenheit
data_dm(:,4) = USHCNdm(dataArray{17});
data_temp(:,5) = FahrenheitToCelsius(dataArray{20}/10);     % In tenth of degrees Fahrenheit
data_dm(:,5) = USHCNdm(dataArray{21});
data_temp(:,6) = FahrenheitToCelsius(dataArray{24}/10);     % In tenth of degrees Fahrenheit
data_dm(:,6) = USHCNdm(dataArray{25});
data_temp(:,7) = FahrenheitToCelsius(dataArray{28}/10);     % In tenth of degrees Fahrenheit
data_dm(:,7) = USHCNdm(dataArray{29});
data_temp(:,8) = FahrenheitToCelsius(dataArray{32}/10);     % In tenth of degrees Fahrenheit
data_dm(:,8) = USHCNdm(dataArray{33});
data_temp(:,9) = FahrenheitToCelsius(dataArray{36}/10);     % In tenth of degrees Fahrenheit
data_dm(:,9) = USHCNdm(dataArray{37});
data_temp(:,10) = FahrenheitToCelsius(dataArray{40}/10);    % In tenth of degrees Fahrenheit
data_dm(:,10) = USHCNdm(dataArray{41});
data_temp(:,11) = FahrenheitToCelsius(dataArray{44}/10);    % In tenth of degrees Fahrenheit
data_dm(:,11) = USHCNdm(dataArray{45});
data_temp(:,12) = FahrenheitToCelsius(dataArray{48}/10);    % In tenth of degrees Fahrenheit
data_dm(:,12) = USHCNdm(dataArray{49});
%tempMonth13 = FahrenheitToCelsius(dataArray{52}/10);    % Annual average
%dmMonth13 = USHCNdm(dataArray{53});


%% Import newly created monthly averages



lengthLat = 32;
lengthLon = 64;
lengthTime = 1788;

% Create empty matrices. Here we use three: one accumulating the temperature measurements, one the
% amount of monthly averages, and one the amount of days used for the average

dataUSHCN = zeros(lengthLon, lengthLat, lengthTime);
numMonthlyAverages = zeros(lengthLon, lengthLat, lengthTime);
numDailyMeasUSHCN = zeros(lengthLon, lengthLat, lengthTime, 'int32');

% Loop for every monthly average for every station, and put it in the correct bins of the above
% matrices. If after 1998, the measurement is ignored. This is 13% of all data

stateRefTempTotal = zeros([length(unique(station_state)) 12]);
stateRefTempNum = zeros([length(unique(station_state)) 12]);

beginYearPerStation = 5000*ones(1,length(station_lat));
endYearPerStation = zeros(1,length(station_lat));

for i = 1:length(data_years)
    
	station_num = station_ids_inv(data_station_ids(i));
    stationLat = station_lat(station_num);
    stationLon = station_lon(station_num);
    
    [latIndex, lonIndex] = nearestGridPointModel(stationLat, stationLon);
    
    if(beginYearPerStation(station_num) > data_years(i))
        beginYearPerStation(station_num) = data_years(i);
    end
    if(endYearPerStation(station_num) < data_years(i))
        endYearPerStation(station_num) = data_years(i);
    end
    
    for month = 1:12
        timeIndex = timeToIndex(data_years(i), month);  % January of that year

        % Do this for every month
        if (timeIndex > 0 && data_temp(i, month) > -70)
            % Accumulate temperature
            dataUSHCN(lonIndex, latIndex, timeIndex) = dataUSHCN(lonIndex, latIndex, timeIndex) + data_temp(i, month);
            % Add one to num of monthly averages per grid point
            numMonthlyAverages(lonIndex, latIndex, timeIndex) = numMonthlyAverages(lonIndex, latIndex, timeIndex) + 1;
            % Add number of daily measurements per grid point
            numDailyMeasUSHCN(lonIndex, latIndex, timeIndex) = numDailyMeasUSHCN(lonIndex, latIndex, timeIndex) + data_dm(i, month);
            
            if (data_years(i) >= 1961 && data_years(i) <= 1990)
                % Add to state reference temperature
                stateNum = stateToNum(station_state(station_num));
                stateRefTempTotal(stateNum, month) = stateRefTempTotal(stateNum, month) + data_temp(i, month);
                stateRefTempNum(stateNum, month) = stateRefTempNum(stateNum, month) + 1;
            end
            
        end
    end
        
end

stateRefTempAvg = stateRefTempTotal ./ stateRefTempNum;

% Calculate average by dividing every grid point by the number of "monthly averages" that have been
% used to create the value for that grid point. So: divide by numMonthlyAverages

dataUSHCN = dataUSHCN ./ numMonthlyAverages;

% Reset every item without any measurements to nan
dataUSHCN(numDailyMeasUSHCN == 0) = NaN;

%% Save state references

state_names = unique(station_state);
save(statereference_output_file, 'state_names', 'stateRefTempAvg', 'beginYearPerStation', 'endYearPerStation');


%% Plot average temperature of every measurement in time

matrixUSHCN = reshape(dataUSHCN, [64*32, 1788])';
matrixUSHCNzeros = matrixUSHCN;
matrixUSHCNzeros(isnan(matrixUSHCN)) = 0;
totalTemp = sum(matrixUSHCNzeros, 2);
totalTempsUsed = sum(~isnan(matrixUSHCN), 2);

averageAllStations = totalTemp ./ totalTempsUsed;

figure(1); plot(averageAllStations);



%% Anomalies

dataUSHCN_anomalies = dataUSHCN; % dataModel will contain anomalies

% Anomalies are calculated by taking average of time values between 1961 and 1990

startYearAverage = 1961;
endYearAverage = 1990;

averages = zeros(lengthLon,lengthLat,12);

% Calculate average for every month
for month = 1:12
    % Select values for this month
    indicesMonth = timeToIndex(1961, month):12:timeToIndex(1990, 12);
    % Now we have to take into account the number of missing values per grid point. To do this, we
    % make a copy of the values and set every missing value to zero. Then we take the sum for every
    % grid point over time. The missing values are then ignored, since they are zero. To find the
    % average, we divide by the number of non missing values per grid point.
    values = dataUSHCN(:,:,indicesMonth);
    copyValues = values;
    copyValues(isnan(values)) = 0;
    % Calculate average per data point
    averagesPerGridpoint = sum(copyValues, 3) ./ sum(~isnan(values), 3);
    % Then we only keep the averages with a minimum number of points
    averagesPerGridpoint(sum(~isnan(values), 3) <= 5) = nan;
    
    averages(:,:,month) = averagesPerGridpoint;
end

save('../../Data/ushcn/averagesRefAnomaliesUSHCN.mat','averages');

% Remove averages from whole dataset, per month
month = 1;
for i = 1:length(dataUSHCN)
    month = month + 1;
    if (month > 12)
        month = 1;
    end
    dataUSHCN_anomalies(:,:,i) = dataUSHCN(:,:,i) - averages(:,:, month);
end

dataUSHCN = dataUSHCN_anomalies;


%% Save data

save(USHCN_output_file, 'dataUSHCN', 'numDailyMeasUSHCN');

toc




