%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Preprocessing of ECA dataset
%
% - Import metadata about stations
% - Import and combine all station data
% - Create monthly averages
% - Average to 5 deg x 5 deg grid, and calculate variance
% - Calculate anomalies
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

process_stations = false;
process_station_data = false;

stations_file = '../../Data/eca/stations.mat';
temperature_file = '/home/kajivarvdw/ECA_Daily/averages/monthlyAverage_06-Mar-2016 11:39:53.txt';
ECA_output_file = '../../Data/eca/data_eca.mat';

%% Import metadata about stations


if (process_stations)
    
    filename = '/home/kajivarvdw/ECA_Daily/stations.txt';
    outputfile = stations_file;
    delimiter = ',';
    startRow = 19;
    formatSpec = '%f%s%s%s%s%f%[^\n\r]';

    % Open file
    fileID = fopen(filename,'r');
    dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines' ,startRow-1, 'ReturnOnError', false);
    fclose(fileID);

    % Get data out of it
    station_ids = dataArray{:, 1};
    station_names = dataArray{:, 2};
    station_countries = dataArray{:, 3};
    station_lat_txt = dataArray{:, 4};
    station_lon_txt = dataArray{:, 5};
    station_height = dataArray{:, 6};
    N = length(station_lat_txt);

    % Clear up some temporary variables
    clearvars filename delimiter startRow formatSpec fileID dataArray ans;

    % Transform DMS latitudes/longitudes to decimal
    station_lat = zeros(size(station_lat_txt));
    station_lon = zeros(size(station_lon_txt));
    for i = 1:N
        station_lat(i) = DMStoDec(station_lat_txt{i});
        station_lon(i) = DMStoDec(station_lon_txt{i});
    end

    % Save useful parts
    save(outputfile, 'N', 'station_ids', 'station_names', 'station_lat', 'station_lon');

else
    load(stations_file);
end


%% Import data for every station


if (process_station_data)

    delimiter = ',';
    startRow = 17;
    formatSpec = '%d%d%d%f%d%[^\n\r]';

    outputfileName = ['/home/kajivarvdw/ECA_Daily/averages/monthlyAverage_' datestr(datetime('now')) '.txt'];
    outputfileID = fopen(outputfileName, 'w');

    tic;
    for i = station_ids'
        id = sprintf('%06d',i);
        filename = ['/home/kajivarvdw/ECA_Daily/TG_STAID' id '.txt'];
        disp(['Reading from ' filename '...']);

        fileID = fopen(filename,'r');
        textscan(fileID, '%[^\n\r]', startRow-1, 'WhiteSpace', '', 'ReturnOnError', false);
        dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'ReturnOnError', false);
        fclose(fileID);

        data_station_id = dataArray{:,1};
        data_time_txt = dataArray{:,3};
        data_temp = dataArray{:,4} / 10;
        data_quality = dataArray{:,5};
        selection = data_quality == 0;

        data_station_id = data_station_id(selection);
        data_time_txt = data_time_txt(selection);
        data_temp = data_temp(selection);
        sizeStation = length(data_time_txt);

        if (sizeStation > 1)

            data_time = zeros(sizeStation, 3);
            month = 1;
            monthAverage = 0;
            monthN = 0;

            firstYear = timeToArray(data_time_txt(1));
            lastYear = timeToArray(data_time_txt(end));
            numYears = lastYear(3) - firstYear(3);
            numMonths = numYears * 12;

            clear 'monthlyAverage';

            %monthlyAverage = zeros(numMonths, 3);

            for j = 1:sizeStation
                data_time(j,:) = timeToArray(data_time_txt(j));
                if (j == 1)
                    currMonth = data_time(j,2);
                elseif (data_time(j,2) ~= currMonth)
                    % Save previous monthly average
                    monthlyAverage(month, 1) = i;
                    monthlyAverage(month, 2:3) = data_time(j-1,1:2);
                    monthlyAverage(month, 4) = monthAverage / monthN;
                    monthlyAverage(month, 5) = monthN;
                    monthAverage = 0;
                    monthN = 0;
                    currMonth = data_time(j,2);
                    month = month + 1;
                end
                if (data_temp(j) > -5000)
                    monthAverage = monthAverage + data_temp(j);
                    monthN = monthN + 1;
                else
                    disp('Error! Found -9999 with quality 0');
                end
            end
            % Save previous monthly average
            monthlyAverage(month, 1) = i;
            monthlyAverage(month, 2:3) = data_time(j-1,1:2);
            monthlyAverage(month, 4) = monthAverage / monthN;
            monthlyAverage(month, 5) = monthN;

            fprintf(outputfileID, '%d, %d, %d, %f, %d\r\n', monthlyAverage');
            disp(['Written ' num2str(length(monthlyAverage)) ' lines to ' outputfileName]);
            toc
        end
    end

    fclose(outputfileID);

    clearvars filename delimiter startRow formatSpec fileID dataArray ans;
    
end

%% Import newly created monthly averages

tic;
delimiter = ',';
formatSpec = '%f%f%f%f%f%[^\n\r]';

fileID = fopen(temperature_file,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN, 'ReturnOnError', false);
fclose(fileID);

monthlyAverages = [dataArray{1:end-1}];

clearvars filename delimiter formatSpec fileID dataArray ans;

lengthLat = 32;
lengthLon = 64;
lengthTime = 1788;

% Create empty matrices. Here we use three: one accumulating the temperature measurements, one the
% amount of monthly averages, and one the amount of days used for the average

dataECA = zeros(lengthLon, lengthLat, lengthTime);
numMonthlyAverages = zeros(lengthLon, lengthLat, lengthTime);
numDailyMeasECA = zeros(lengthLon, lengthLat, lengthTime, 'int32');

% Create a hash table for the station_ids, which is much faster than using 'find'
station_ids_inv = zeros(max(station_ids), 1, 'int32');
for i = 1:length(station_ids)
    station_ids_inv(station_ids(i)) = i;
end

% Loop for every monthly average for every station, and put it in the correct bins of the above
% matrices. If prior to Jan 1850, the measurement is ignored. This is only 0.3% of all data

for i = 1:length(monthlyAverages)
    
    meas = monthlyAverages(i,:);
	station_num = station_ids_inv(meas(1));
    stationLat = station_lat(station_num);
    stationLon = station_lon(station_num);
    
    [latIndex, lonIndex] = nearestGridPointModel(stationLat, stationLon);
    timeIndex = timeToIndex(meas(2), meas(3));
    
    if (timeIndex > 0)
        % Accumulate temperature
        dataECA(lonIndex, latIndex, timeIndex) = dataECA(lonIndex, latIndex, timeIndex) + meas(4);
        % Add one to num of monthly averages per grid point
        numMonthlyAverages(lonIndex, latIndex, timeIndex) = numMonthlyAverages(lonIndex, latIndex, timeIndex) + 1;
        % Add number of daily measurements per grid point
        numDailyMeasECA(lonIndex, latIndex, timeIndex) = numDailyMeasECA(lonIndex, latIndex, timeIndex) + meas(5);
    end
        
end

% Calculate average by dividing every grid point by the number of "monthly averages" that have been
% used to create the value for that grid point. So: divide by numMonthlyAverages

dataECA = dataECA ./ numMonthlyAverages;

% Reset every item without any measurements to nan
dataECA(numDailyMeasECA == 0) = NaN;


%% Anomalies

dataECA_anomalies = dataECA; % dataModel will contain anomalies

% Anomalies are calculated by taking average of time values between 1961 and 1990

startYearAverage = 1961;
endYearAverage = 1990;

averages = zeros(lengthLon,lengthLat,12);

% Calculate average for every month
for month = 1:12
    % Select values for this month
    indicesMonth = timeToIndex(1961, month):12:timeToIndex(1990, 12);
    % Now we have to take into account the number of missing values per grid point. To do this, we
    % make a copy of the values and set every missing value to zero. Then we take the sum for every
    % grid point over time. The missing values are then ignored, since they are zero. To find the
    % average, we divide by the number of non missing values per grid point.
    values = dataECA(:,:,indicesMonth);
    copyValues = values;
    copyValues(isnan(values)) = 0;
    % Calculate average per data point
    averagesPerGridpoint = sum(copyValues, 3) ./ sum(~isnan(values), 3);
    % Then we only keep the averages with a minimum number of points
    averagesPerGridpoint(sum(~isnan(values), 3) <= 5) = nan;
    
    averages(:,:,month) = averagesPerGridpoint;
end

save('../../Data/eca/averagesRefAnomaliesECA.mat','averages');

% Remove averages from whole dataset, per month
month = 1;
for i = 1:length(dataECA)
    month = month + 1;
    if (month > 12)
        month = 1;
    end
    dataECA_anomalies(:,:,i) = dataECA(:,:,i) - averages(:,:, month);
end

dataECA = dataECA_anomalies;


%% Save data

save(ECA_output_file, 'dataECA', 'numDailyMeasECA');

toc
