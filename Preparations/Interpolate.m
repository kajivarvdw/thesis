function dataNew = Interpolate (dataOrig, latNew, lonNew, onlyNearestNeighbour)
%INTERPOLATE Returns the linear interpolation of a 2D grid, taking into account missing data points

if nargin < 4
    onlyNearestNeighbour = false;
end

dataNew = NaN(length(lonNew), length(latNew));

% Loop over all new grid points
for j = 1:length(latNew)
    for i = 2:length(lonNew)-1
        x = lonNew(i);
        y = latNew(j);
        newValue = NaN;
        [coordinates, points] = nearestGridPoints (x, y);
        %%%%%%%%%%%%%%%%%%%%%%%%
        %   Q12 ------ Q22     %
        %    |          |      %
        %    |       * (x,y)   %
        %    |          |      %
        %   Q11 ------ Q21     %
        %%%%%%%%%%%%%%%%%%%%%%%%
        
        fQ11 = dataOrig(points(1,1), points(1,2));
        fQ21 = dataOrig(points(2,1), points(2,2));
        fQ12 = dataOrig(points(3,1), points(3,2));
        fQ22 = dataOrig(points(4,1), points(4,2));
        
        x1 = coordinates(1,1); y1 = coordinates(1,2);
        x2 = coordinates(4,1); y2 = coordinates(4,2);
        
        % First check if all values exist
        if (~(isnan(fQ11) || isnan(fQ21) || isnan(fQ12) || isnan(fQ22)) && ~onlyNearestNeighbour)
            % Perform bilinear interpolation
            newValue = (fQ11*(x2 - x)*(y2 - y) + fQ21*(x - x1)*(y2 - y) + fQ12*(x2 - x)*(y - y1) + fQ22*(x - x1)*(y - y1)) / ((x2 - x1)*(y2-y1));
        else
            
            % Check if there are three existing points
            if (sum(isnan([fQ11, fQ21, fQ12, fQ22])) == 3)
                % Add weighted versions of every existing point
                totalArea = 0;
                totalAverage = 0;
                if (~isnan(fQ11))
                    totalArea = totalArea + (x2 - x)*(y2 - y);
                    totalAverage = totalAverage + fQ11 * (x2 - x)*(y2 - y);
                end
                if (~isnan(fQ21))
                    totalArea = totalArea + (x - x1)*(y2 - y);
                    totalAverage = totalAverage + fQ21 * (x - x1)*(y2 - y);
                end
                if (~isnan(fQ12))
                    totalArea = totalArea + (x2 - x)*(y - y1);
                    totalAverage = totalAverage + fQ12 * (x2 - x)*(y - y1);
                end
                if (~isnan(fQ22))
                    totalArea = totalArea + (x - x1)*(y - y1);
                    totalAverage = totalAverage + fQ22 * (x - x1)*(y - y1);
                end
                
                newValue = totalAverage / totalArea;
                
            else
            
                % Check if closest two points exist
                [closestTwo, closestCoordinates] = closestTwoPoints([x y], points, coordinates);

                fQ1 = dataOrig(closestTwo(1,1), closestTwo(1,2));
                fQ2 = dataOrig(closestTwo(2,1), closestTwo(2,2));

                if (~(isnan(fQ1) || isnan(fQ2)) && ~onlyNearestNeighbour) % Both points exist
                    % Check wheter we have horizontal or vertical interpolation
                    if (closestTwo(1,1) ~= closestTwo(2,1))
                        % Horizontal linear interpolation
                        x1 = closestCoordinates(1,1);
                        x2 = closestCoordinates(2,1);
                        newValue = ((x2 - x)*fQ1 + (x - x1)*fQ2) / (x2 - x1);
                    else
                        % Vertical linear interpolation
                        y1 = closestCoordinates(1,2);
                        y2 = closestCoordinates(2,2);
                        newValue = ((y2 - y)*fQ1 + (y - y1)*fQ2) / (y2 - y1);
                    end
                else
                    % Perform nearest neighbour interpolation
                    newValue = fQ1;
                end
                
            end
            
        end
        
        dataNew(i, j) = newValue;
            
    end
end

end







