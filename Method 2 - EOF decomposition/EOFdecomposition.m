%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Accuracy analysis of LOVECLIM climate model in comparison to HadCRUT measurements
%
% Bachelor thesis Kaj-Ivar van der Wijst
% Under supervision of Svetlana Dubinkina, CWI, Amsterdam
% November 2015
%
%
% EOF Decomposition
% -----------------------------
%
% This file calculates the EOF decomposition of dataModel and dataMeasurement
% from the file data.mat. The resulting EOFs and PCs are saved to an output
% file. Specifically, these variables:
%    * modelEOFs / dataEOFs         (EOFs as column vectors)
%    * modelPCs / dataPCs           (PCs as column vectors)
%    * modelVariancePercentages / dataVariancePercentages   (Eigenvalues in percentage)
%    * analyzedField                (reconstructed field after DINEOF analysis)
% The variables matrixModel and matrixMeasurement are simply reshaped versions of dataModel /
% dataMeasurement.
%
%
%
% - EOF Decomposition of measurements using DINEOF
%       - Either use model validation or measurements
%       - Determine optimal number of EOFs for truncation
%       - Perform DINEOF using DINEOF.m
% - EOF Decomposition of model
%       - Either use SVD or eigenvalue decomposition
% - Save output
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Options

dataFolder = GetDataFolder();


weightedLatitude = true;        % Multiply every value by the square root of the cosine
                                % of the latitude to account for extra values near the poles
                            
determineOptimalEOFs = false;   % Run an algorithm to determine the optimal number of EOFs
                                % during truncation in the DINEOF process

useSVD = true;                  % Use Singular Value Decomposition instead of Eigenvalues to
                                % calculate the EOFs/PCs
                                
extraPlots = false;              % Make a plot of optimal num of EOFs for truncation, comparison
                                % of DINEOF reconstruction, extra plot of EOF's/PC's
                                
saveOutput = true;             % Save output to file

assimilated = false;     % Instead of model, use assimilated model. This is created by DataAssimilation.m

if(assimilated)
    sigmas = [2 1 0.5 0.25 0.1 0.05 0.01];
else
    sigmas = 1;
end

% Can be either 'north', 'south' or 'world'
for hemisphere = {'north'}      
    
% Average over n years. If 0, no average (monthly measurements)
for nYearAverage = [0 1 2 5 10 20]
    
% Perform DINEOF on model where values are removed, and compare
% it to original model, to get an estimate of how good DINEOF
% performs  
for DINEOFvalidationMode = [false] 
    
% This loop is only necessary when calculating EOFs of assimilated fields                               
for sigmaModel = sigmas         

                            

%% Import data

clock = tic;

disp(['Analyzing for hemisphere=' hemisphere{1} ', nYearAverage=' num2str(nYearAverage) ', DINEOF validation mode = ' num2str(DINEOFvalidationMode) '...']);

logMsg('Importing data...', clock);
load([dataFolder 'data.mat']);
if (assimilated)
    nYearAverageTmp = nYearAverage;
    load([dataFolder 'assimilated/data_assimilated_sigma' num2str(sigmaModel) '_' num2str(nYearAverageTmp) 'yearAverage.mat']);
    nYearAverage = nYearAverageTmp;
    dataModel = dataAssimilated;
end

n = length(time);

% Choose a hemisphere
if (strcmp(hemisphere, 'north'))
    dataModel = dataModel(:,17:end,:);
    dataMeasurement = dataMeasurement(:,17:end,:);
    lat = lat(17:end);
elseif (strcmp(hemisphere, 'south'))
    dataModel = dataModel(:,1:16,:);
    dataMeasurement = dataMeasurement(:,1:16,:);
    lat = lat(1:16);
end

p = length(lat)*length(lon);
numLat = length(lat);
pOrig = p;
    

% Multiply anomalyField by square root of cosine of latitude (as done in Hannachi2006)
weightedModel = dataModel;

if (weightedLatitude)
    weight = sqrt(cos(lat*pi/180));
    for i = 1:size(dataModel, 3)
        weightedModel(:,:,i) = weightedModel(:,:,i) * diag(weight);
    end
end
 
% Reshape data to n x p matrix, with n=1788 time intervals and p=2048 spacial grid points
matrixModelOrig = reshape(weightedModel,[p size(dataModel, 3)])';


%% EOF Decomposition of data (DINEOF)

logMsg('Creating anomaly field and sample covariance for data...', clock);

% Put a 0 in the matrix for every missing value
missingValues = isnan(dataMeasurement);
if (DINEOFvalidationMode)
    observedField = dataModel;
    observedField(missingValues) = nan;
else
    observedField = dataMeasurement;
end
observedField(missingValues) = 0;                           % Called X_0 in Beckers2003

% Multiply anomalyField by square root of cosine of latitude (as done in Hannachi2006)
if (weightedLatitude)
    weight = sqrt(cos(lat*pi/180));
    for i = 1:n
        observedField(:,:,i) = observedField(:,:,i) * diag(weight);
    end
end

thresholdDINEOF = 0.02;                                     % Relative difference after which DINEOF loop stops
rMax = 30;                                                  % Max number of loops before forced stop, even if threshold isn't reached

if (determineOptimalEOFs)
    
    % 0-100: every 5 EOFs, 100-500: every 25, 500-1500: every 100
    testRange = [1 5:5:99 100:25:499 500:100:1500];
    
    [Noptimal, rmsDistanceNum, rmsDistanceVal, nTest] = optimalEOFsDINEOF(...
        observedField, missingValues, p, n, rMax, thresholdDINEOF, testRange, clock, numLat...
    );
    observedFieldOriginal = observedField;
    save([dataFolder 'optimalNumEOFs/optimalEOFs.mat'], 'Noptimal', 'rmsDistanceNum', 'rmsDistanceVal', 'observedFieldOriginal', 'missingValues', 'p', 'n', 'rMax', 'thresholdDINEOF', 'testRange', 'nTest');
    
else
    
    missingValuesOrig = missingValues;
    if (DINEOFvalidationMode)
        load([dataFolder 'optimalNumEOFs/optimalEOFs_validationModel.mat']);
    else
        load([dataFolder 'optimalNumEOFs/optimalEOFs.mat']);
    end
    p = pOrig;
    missingValues = missingValuesOrig;
    
end

if (extraPlots)
    % Plot optimal EOF:
    figure(1);
    ax = gca;
    plot(rmsDistanceNum, rmsDistanceVal);
    line([Noptimal Noptimal], get(ax, 'YLim'), 'color', 'red');
    ax.XTick = [0 Noptimal 500 1000 1500];
    %title('Optimal number of EOFs for DINEOF truncation');
    ylabel('RMS error');
    xlabel('Number of EOFs for truncation');
    drawnow;
end

N = Noptimal;

logMsg('Calculating EOFs and PCs for data using DINEOF...', clock);


[dataEOFs, dataPCs, dataVariancePercentages, analyzedField] = DINEOF(observedField, missingValues, p, n, N, rMax, thresholdDINEOF, clock, numLat);

% We'll only be using the first EOF / PC
firstPercentageData = dataVariancePercentages(1);
firstEOFdata = dataEOFs(:,1);
firstPCdata = dataPCs(:,1);

if (extraPlots)
    % Plot reconstructed observed field
    figure(2);
    imagesc(analyzedField(:,:,601)', [-3 3]);
    title('Reconstructed field in Jan 1900');
    figure(3);
    tmp = observedField;
    tmp(missingValues) = nan;
    imagesc(tmp(:,:,601)', [-3 3]);
    title('Observed field in Jan 1900');
    if (DINEOFvalidationMode)
        figure(4);
        imagesc(weightedModel(:,:,601)', [-3 3]);
        caxis([-3 3]);
        title('Original field in Jan 1900');
    end
end



%% Calculate n year average (if necessary)

if (nYearAverage > 0)
    
    % We ignore every additional months which doesn't fit into the n-year bins

    nMonthAverage = nYearAverage * 12;
    totalBins = int16(floor(n / nMonthAverage));

    averagingMatrix = zeros(totalBins, n);

    for b = 0:totalBins-1
        averagingMatrix(b+1, b*nMonthAverage+1:(b+1)*nMonthAverage) = 1;
    end
    
    averagingMatrix = (1/nMonthAverage) .* averagingMatrix;
    
    if (assimilated)
        matrixModel = matrixModelOrig; 
    else
        matrixModel = averagingMatrix * matrixModelOrig;
    end
    
    % Take average of measurements
    matrixMeasurementOrig = reshape(analyzedField, [p n])';
    matrixMeasurement = averagingMatrix * matrixMeasurementOrig;
    n = double(totalBins);
    
    
else
    matrixModel = matrixModelOrig;
    matrixMeasurement = reshape(analyzedField, [p n])';
end

%% Rerun DINEOF on averaged data set

if (nYearAverage > 0)
    
    centeringMatrix = eye(n) - ones(n, n) ./ n;                  % Called H
    anomalyFieldData = centeringMatrix * matrixMeasurement;                % Called X
    
    [A, gamma, U] = svd(anomalyFieldData);

    dataEOFs = U;
    dataPCs = A * diag(diag(gamma));
    dataVariancePercentages = diag(gamma).^2 / sum(diag(gamma).^2);

    % We'll only be using the first EOF / PC
    firstPercentageData = dataVariancePercentages(1);
    firstEOFdata = dataEOFs(:,1);
    firstPCdata = dataPCs(:,1);
end



%% EOF Decomposition of model

logMsg('Creating anomaly field and sample covariance for model...', clock);

%matrixModel = reshape(analyzedField,[p n])';
centeringMatrix = eye(n) - ones(n, n) ./ n;                  % Called H
anomalyField = centeringMatrix * matrixModel;                % Called X


if (useSVD)
    
    logMsg('Calculating EOFs and PCs for model using SVD...', clock);
    
    [A, gamma, U] = svd(anomalyField);

    modelEOFs = U;
    if (length(A) < size(gamma, 2))
        gamma = diag(diag(gamma));
    end
    modelPCs = A * gamma;
    modelVariancePercentages = diag(gamma).^2 / sum(diag(gamma).^2);
    
else
    
    logMsg('Calculating EOFs and PCs for model using eigenvalues...', clock);
    
    sampleCovariance = anomalyField' * anomalyField ./ n;   % Called S

    [eigenvectors, eigenvalueMatrix] = eig(sampleCovariance);
    % Flip eigenvectors to get biggest eigenvalues first
    eigenvectors = fliplr(eigenvectors);                    % Columns called u_k
    eigenvalues = flipud(diag(eigenvalueMatrix));           % Elements called lambda^2_k
    % These eigenvalues are the square of the actual eigenvalues

    modelEOFs = eigenvectors;
    modelPCs = anomalyField * modelEOFs;
    modelVariancePercentages = eigenvalues ./ sum(eigenvalues);
    
end

% We'll only be using the first EOF / PC
firstPercentageModel = modelVariancePercentages(1);
firstEOFmodel = modelEOFs(:,1);
firstPCmodel = modelPCs(:,1);


%% Save EOF's / PC's of model and data

if (saveOutput)
    filename = 'EOFsPCs';
    if (DINEOFvalidationMode)
        filename = [filename 'DINEOFvalidation'];
    end
    if (nYearAverage > 0)
        filename = [filename '_' num2str(nYearAverage) 'yearAverage'];
    end
    filename = [filename '_' hemisphere{1} '.mat'];
    
    filesize = ByteSize({analyzedField, matrixMeasurement, dataEOFs, dataPCs, dataVariancePercentages, modelEOFs, modelPCs, modelVariancePercentages});
    
    if (assimilated)
        assimilatedDir = 'Assimilated/';
        filename = ['sigma' num2str(sigmaModel) '_' filename];
    else
        assimilatedDir = '';
    end
    logMsg(['Saving to ' dataFolder 'EOFsPCs/' assimilatedDir filename ' (size: ' filesize ')...'], clock);
    save([dataFolder 'EOFsPCs/' assimilatedDir filename],...
        'useSVD', 'DINEOFvalidationMode', 'weightedLatitude',...       % General options
        'n', 'p', 'nYearAverage',...
        'thresholdDINEOF', 'rMax', 'Noptimal',...
        'analyzedField', 'matrixMeasurement', 'dataEOFs', 'dataPCs', 'dataVariancePercentages',...
        'modelEOFs', 'modelPCs', 'modelVariancePercentages', 'lat', 'lon', 'hemisphere', 'matrixModel', 'sigmaModel', 'assimilated');
end





%% Finalize

logMsg('Done.', clock);

end
end
end
end

