function [ eof, pc, variancePerc, varargout ] = DINEOF( observedField, missingValues, p, n, N, rMax, threshold, clock, numLat )
%DINEOF Performs EOF recovery of missing values using DINEOF process
%   
%   Input arguments:
%       - observedField: initial guess: anomaly field with 0 at missing values
%       - missingValues: elements of observedField that are missing
%       - p, n: number of spatial points and time points
%       - N: number of EOFs to be used in truncation
%       - rMax: maximum number of DINEOF iteration 
%       - threshold: relative difference after which DINEOF loop stops
%       - clock: tic object used for logging times
%

% Begin iterations with analyzedField = observedField: initial guess
analyzedField = observedField;

for r = 1:rMax
    logMsg(['Starting round ' num2str(r) ' of DINEOF process...'], clock);

    % Apply SVD on X_0 or X_a
    prev = analyzedField;                                   % Save current state for error comparison
    matrixData = reshape(analyzedField,[p n])';
    [dataA, dataGamma, dataU] = svd(matrixData);            % Note: Shouldn't we do centring first?
    
    % Truncate EOF series: U_n * D_n * (V_n)'
    truncated = dataA(:,1:N) * dataGamma(1:N,1:N) * (dataU(:,1:N)');
    reshapedTruncated = reshape(truncated, [64 numLat n]);

    % Replace missing values in original data by values just calculated with truncated EOFs
    analyzedField(missingValues) = reshapedTruncated(missingValues);
    
    % Check if convergence is reached (relative difference below threshold percentage)
    diff = abs(analyzedField - prev);
    total = abs(analyzedField);
    errorDINEOF(r) = sum(diff(:)) / sum(total(:));
    logMsg(['Current rel. diff.: ' num2str(errorDINEOF(r))], clock);
    if (errorDINEOF(r) < threshold)
        break;
    end
end

eof = dataU;
if (length(dataA) < size(dataGamma, 2))
    dataGamma = diag(diag(dataGamma));
end
pc = dataA * dataGamma;
variancePerc = diag(dataGamma).^2 / sum(diag(dataGamma).^2);

% If fourth output var is given, return also the reconstructed field
if nargout >= 4
    varargout{1} = analyzedField;
end

if nargout == 5
    varargout{2} = dataGamma;
end


% Plot
% figure(7); imagesc(analyzedField(:,:,10), [-3 3]);
% figure(6); imagesc(observedField(:,:,10), [-3 3]);


end

