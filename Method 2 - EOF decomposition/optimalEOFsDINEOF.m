function [ optimalNumber, rmsDistanceNum, rmsDistanceVal, sizeSample ] = optimalEOFsDINEOF( observedField, missingValues, p, n, rMax, thresholdDINEOF, testRange, clock, numLat )
%OPTIMALEOFSDINEOF Calculates the optimal number of EOFs

logMsg('Determining optimal number of EOFs for truncation in DINEOF...', clock);

% Start with choosing 0.2% of all values as test values
% (0.2% because by taking them random, we expect to keep 0.1% of all values (~50% missing))
nTest = int32(0.002 * n*p);
sample = randsample(1:n*p, nTest);
sampleIndex = zeros(n*p,1,'int32');
sampleIndex(sample) = 1;

% Filter missing values from this sample
sampleIndex = sampleIndex - int32(missingValues(:));
sampleIndex(sampleIndex < -0.5) = 0;
nTest = sum(sampleIndex);

% Save chosen values
sampleValues = observedField(find(sampleIndex));

testField = observedField;
testField(find(sampleIndex)) = 0;
testMissingValues = missingValues;
testMissingValues(find(sampleIndex)) = 1;

rmsDistance = zeros(length(testRange), 1);

% Perform DINEOF on test field with various number of EOFs
i = 1;
for Ntest = testRange
    logMsg(['Starting test with ' num2str(Ntest) ' EOFs...'], clock);
    [~, ~, ~, newTestField] = DINEOF(testField, testMissingValues, p, n, Ntest, rMax, thresholdDINEOF, clock, numLat);

    newSampleValues = newTestField(find(sampleIndex));
    rmsDistance(i) = sqrt(sum((sampleValues - newSampleValues).^2) / nTest);
    logMsg(['RMS distance with ' num2str(Ntest) ' EOFs: ' num2str(rmsDistance(i))], clock);
    logMsg('------------------------------------', clock);
    i = i + 1;
end

figure(1);
plot(testRange, rmsDistance);
drawnow;
[~, minIndex] = min(rmsDistance);

% Output
optimalNumber = testRange(minIndex);
rmsDistanceNum = testRange;
rmsDistanceVal = rmsDistance;
sizeSample = nTest;


end

