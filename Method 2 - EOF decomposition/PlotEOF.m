%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Accuracy analysis of LOVECLIM climate model in comparison to HadCRUT measurements
%
% Bachelor thesis Kaj-Ivar van der Wijst
% Under supervision of Svetlana Dubinkina, CWI, Amsterdam
% November 2015
%
%
% Plot EOF decomposition
% -----------------------------
%
% - Plot Model EOFs, either on world map or stereographic projection
% - Plot Data EOFs
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function PlotEOF (filename)

dataFolder = GetDataFolder();


%% Options

plotHadCRUT = true;
plotModel = true;
saveFigures = true;

contourPlotFilled = false;  % False: plot only contour lines; true: filled contour plot

if (nargin == 0)
    filename = 'EOFsPCs_10yearAverage_south.mat';
end

inputfile = [dataFolder 'EOFsPCs/' filename];

assimilatedModel = false;   % Just used for the filename of the plot
beginFigID = 13;



%% Import data

L = load(inputfile);

% Load variables from file
hemisphere = L.hemisphere;
DINEOFvalidationMode = L.DINEOFvalidationMode;
nYearAverage = L.nYearAverage;
%useSVD = L.useSVD;
weightedLatitude = L.weightedLatitude;

%thresholdDINEOF = L.thresholdDINEOF;
%rMax = L.rMax;
%Noptimal = L.Noptimal;

lat = L.lat;
lon = L.lon;
n = L.n;        p = L.p;


if (isfield(L,'ushcneca'))
    ushcneca = L.ushcneca;
else
    ushcneca = false;
end

dataEOFs = L.dataEOFs;                                      modelEOFs = L.modelEOFs;
dataPCs = L.dataPCs;                                        modelPCs = L.modelPCs;
dataVariancePercentages = L.dataVariancePercentages;        modelVariancePercentages = L.modelVariancePercentages;
%matrixMeasurement = L.matrixMeasurement;
%analyzedField = L.analyzedField;



%% Plot

latOrig = lat;
lonOrig = lon;
[lonMesh, latMesh] = meshgrid(lonOrig, latOrig);
lonMesh(:,end) = 360;
numLat = length(latOrig);

L2 = load('coast');
lat = L2.lat;
long = L2.long;


% Define various extra texts
textForWeighted = '';
if (~weightedLatitude)
    textForWeighted = ', (not weighted)';
end
textForAverage = '';
if (nYearAverage > 0)
    textForAverage = ['Average over: ' num2str(nYearAverage) ' years'];
end
nameData = 'HadCRUT data';
if (DINEOFvalidationMode)
    nameData = 'model (reconstructed)';
end
if (ushcneca)
    nameData = 'USHCN ECA data';
end
nameModel = 'model';
if (assimilatedModel)
    nameModel = 'assimilated model';
    if (ushcneca)
        nameModel = 'assimilated (USHCN ECA) model';
    end
end
preAveragedTxt = '';


%% Plot EOF's 1 and 2 on world map or stereographic projection

if (contourPlotFilled)
    filledNum = 1;
else
    filledNum = 0;
end

if (plotModel)
    plotContours (modelEOFs, modelVariancePercentages, nameModel, beginFigID);
end
if (plotHadCRUT)
    plotContours (dataEOFs, dataVariancePercentages, nameData, beginFigID + 2);
end

function plotContours (data, variance, text, figID)
    
    lonNew = [lonOrig(33:end)' - 360 lonOrig(1:32)']';
    % S = shaperead('landareas.shp');
    eofnum = [[1, filledNum]; [2, filledNum]];
    for q = 1:length(eofnum)
        figure(figID + q);
        clf;
        if (strcmp(hemisphere, 'north'))
            axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [90 0 0], 'MapLatLimit', [15 90]);
        elseif (strcmp(hemisphere, 'south'))
            axesm('breusing', 'Frame', 'on', 'Grid', 'on', 'Origin', [-90 0 0], 'MapLatLimit', [-90 -10]);
        else
            axesm eckert4;
            framem; gridm; axis off;
        end
        % Change latitude to -180 : 180
        tmp = reshape(data(:,eofnum(q,1)),[64 numLat])';
        tmp = [tmp(:,33:end) tmp(:,1:32)];
        lonNewTmp = lonNew;
        lonNewTmp(end) = 180; 
        hold on;
        %geoshow(S, 'EdgeAlpha', 0, 'FaceColor', [.7 .7 .7]);
        if ( eofnum(q, 2) == 1)
            setm(gca, 'GColor', [0.9 0.9 0.9]); 
            contourfm(double(latOrig), double(lonNewTmp), tmp, 12, 'LineStyle', 'none');
            geoshow(lat, long, 'Color', [1 1 1 .5]);
        else
            setm(gca, 'GColor', [0.3 0.3 0.3]);
            geoshow('landareas.shp', 'EdgeAlpha', 0, 'FaceColor', [.7 .7 .7]);
            contourm(double(latOrig), double(lonNewTmp), tmp, 12, 'LineWidth', 1.5);
        end
        caxis([-0.65*max(abs(tmp(:))), 0.65*max(abs(tmp(:)))]);
        %geoshow(lat, long, 'Color', [1 1 1 .5]);
        %geoshow('landareas.shp', 'EdgeColor', [1 1 1], 'EdgeAlpha', .5, 'FaceColor', [1 1 1], 'FaceAlpha', 0);
        %geoshow('landareas.shp', 'EdgeAlpha', 0, 'FaceColor', [.8 .8 .8])
        %contourm(double(latOrig), double(lonNewTmp), tmp, 12, 'LineWidth', 1.5);
        axis off;
        set(gcf, 'Color', 'w');
        titlestr = sprintf('EOF %d of %s (%2.2f%% of var.)\n%s %s', eofnum(q,1), text, variance(eofnum(q,1))*100, textForAverage, textForWeighted);
        title(titlestr);
        drawnow;
        imgname = sprintf('EOF%d_%dyearAverage%s_%s_%s.png',eofnum(q,1),nYearAverage,preAveragedTxt,strrep(text,' ','_'),hemisphere{1});
        if (saveFigures)
            export_fig(imgname, '-m2.5');
        end
    end
    
end

end
