%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Accuracy analysis of LOVECLIM climate model in comparison to HadCRUT measurements
%
% Bachelor thesis Kaj-Ivar van der Wijst
% Under supervision of Svetlana Dubinkina, CWI, Amsterdam
% November 2015
%
%
% EOF Projection Analysis
% -----------------------------
%
% - Import data
%       * data.mat
%       * [EOF file].mat
% - Data on model
%       - First applying centering to data (X' instead of X in notation of Hannachi)
%       - Then project data on EOFs of model
%       - Calculate correlation & RMS, and plot first PCs
% - Model on data
%       - First applying centering to model (X' instead of X in notation of Hannachi)
%       - Then project model on EOFs of data
%       - Calculate correlation & RMS, and plot first PCs
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


dataOnModel = true;

eofNums = [1];              % Which EOFs/PCs should be compared?
savePDF = false;
savePNG = false;

assimilatedModel = true;    % Only used for image file name

compareWithOrig = true;     % Only used for data assimilation


dataFolder = GetDataFolder();

% filenames = {'EOFsPCs_north.mat', 'EOFsPCs_south.mat',...
%              'EOFsPCs_1yearAverage_north.mat', 'EOFsPCs_1yearAverage_south.mat',...
%              'EOFsPCs_2yearAverage_north.mat', 'EOFsPCs_2yearAverage_south.mat',...
%              'EOFsPCs_5yearAverage_north.mat', 'EOFsPCs_5yearAverage_south.mat',...
%              'EOFsPCs_10yearAverage_north.mat', 'EOFsPCs_10yearAverage_south.mat',...
%              'EOFsPCs_20yearAverage_north.mat', 'EOFsPCs_20yearAverage_south.mat'};
% filenames = {'Assimilated/sigma2_EOFsPCs_20yearAverage_north.mat',...
%              'Assimilated/sigma1_EOFsPCs_20yearAverage_north.mat',...
%              'Assimilated/sigma0.5_EOFsPCs_20yearAverage_north.mat',...
%              'Assimilated/sigma0.25_EOFsPCs_20yearAverage_north.mat',...
%              'Assimilated/sigma0.1_EOFsPCs_20yearAverage_north.mat',...
%              'Assimilated/sigma0.05_EOFsPCs_20yearAverage_north.mat',...
%              'Assimilated/sigma0.01_EOFsPCs_20yearAverage_north.mat'};
filenames = {'Assimilated/sigma0.2_EOFsPCs_10yearAverage_north.mat'};

corrCoefficientsModelOnData = zeros(size(filenames));
rmsErrorsModelOnData = zeros(size(filenames));

corrCoefficientsDataOnModel = zeros(size(filenames));
rmsErrorsDataOnModel = zeros(size(filenames));

for q = 1:numel(filenames);
    
%clear

filename = filenames{q};

%% Import data

clock = tic;

logMsg('Importing data...', clock);
load([dataFolder 'data.mat']);
load([dataFolder 'EOFsPCs/' filename]);

if (assimilated && compareWithOrig)
    if (nYearAverage > 0)
        L = load(sprintf(['%sEOFsPCs/EOFsPCs_' num2str(nYearAverage) 'yearAverage_%s.mat'],dataFolder,hemisphere{1}));
    else
        L = load(sprintf('%sEOFsPCs/EOFsPCs_%s.mat',dataFolder,hemisphere{1}));
    end
    matrixModelOrig = L.matrixModel;
    modelEOFsOrig = L.modelEOFs;
    colorOrig = [0.7 0.7 0.7];
end


%% Project the data onto the model EOFs

if (dataOnModel)
    logMsg('Calculating projection of data onto model EOFs...', clock);

    centeringMatrix = eye(n) - ones(n, n) ./ n;
    %matrixData = reshape(analyzedField, [p n])';
    matrixData = matrixMeasurement;

    anomalyFieldData = centeringMatrix * matrixData;

    projectedPCs = anomalyFieldData * modelEOFs;

    if (assimilated && compareWithOrig)
        projectedPCsOrig = anomalyFieldData * modelEOFsOrig;
    end


    %% Analysis of pre-averaged data

    for i = eofNums
        f = figure(i); clf;
        xlabel('Year');
        ylabel('Principal component');
        figuresize(380, 190, 'points');
        iD = num2str(i);
        if (assimilatedModel)
            assimilatedExtra = [' (assim., \sigma ' num2str(sigmaModel) ')'];
            assimilatedFile = ['_assimilated_sigma' num2str(sigmaModel)];
            modelName = 'assimilated model';
        else
            assimilatedExtra = '';
            assimilatedFile = '';
            modelName = 'model';
        end
        legendLabels = {['PC ' iD ' projected data'], ['PC ' iD ' ' modelName]};
        hold on;
        if (nYearAverage > 0)
            x=1850+nYearAverage:nYearAverage:1999;
        else
            x=1:length(time);
            set(gca,'xTick',1:300:1801)
            set(gca,'xTickLabel',{'1850', '1875', '1900', '1925', '1950', '1975','2000'})
            axis([1 1801 -inf inf]);
        end
        plot(x,projectedPCs(:,i));
        %plot(x,dataPCs(:,i));
        plot(x,modelPCs(:,i));
        if (assimilated && compareWithOrig)
            plot(x,projectedPCsOrig(:,i),'--','Color',colorOrig);
        end
        legend(legendLabels, 'Location', 'best');
        corrPCmat = corrcoef(projectedPCs(:,i),modelPCs(:,i));
        corrPC = corrPCmat(1,2);
        corrCoefficientsDataOnModel(q) = corrPC;
        rmsErrorsDataOnModel(q) = rms(projectedPCs(:,i) - modelPCs(:,i));
        if (nYearAverage > 0)
            titlestr = sprintf('PC %d of %d year average, hemisph.: %s%s\nCorrelation projected/model: %1.3f\nHadCRUT data projected on %s' ,i, nYearAverage, hemisphere{1}, assimilatedExtra, corrPC, modelName);
        else
            titlestr = sprintf('PC %d (no average), hemisph: %s%s\nCorrelation projected/model: %1.3f\nHadCRUT data projected on %s' ,i, hemisphere{1}, assimilatedExtra, corrPC, modelName);
        end
        title(titlestr);
        set(gcf, 'Color', 'w');
        drawnow;
        if (nYearAverage > 0)
            imgname = sprintf('PC%d%s_%dyearAverage_%s',i,assimilatedFile,nYearAverage,hemisphere{1});
        else
            imgname = sprintf('PC%d%s_%s',i,assimilatedFile,hemisphere{1});
        end
        if (savePNG)
            drawnow;
            export_fig([imgname '.png'], '-m2.5');
        end
        if (savePDF)
            drawnow;
            print([imgname '.pdf'], '-dpdf');
        end
    end

    %load coast;
    %figure(5); clf; geoshow(reshape(modelEOFs(:,1), [64 32])', [1/5.6250 90 0], 'DisplayType', 'texturemap');
    %geoshow(lat, long);
    %caxis([-0.1 0.1]);


end


%% Project the model onto the data EOFs

logMsg('Calculating projection of model onto data EOFs...', clock);

centeringMatrix = eye(n) - ones(n, n) ./ n;
%matrixModel = reshape(dataModel, [size(matrixData, 1) n])';
%matrixData = matrixMeasurement;

anomalyFieldModel = centeringMatrix * matrixModel;

projectedPCs = anomalyFieldModel * dataEOFs;

if (assimilated && compareWithOrig)
    projectedPCsOrig = (centeringMatrix * matrixModelOrig) * dataEOFs;
end


%% Analysis of pre-averaged data
    
for i = eofNums
    f = figure(i+2); clf;
    figuresize(380, 170, 'points');
    xlabel('Year');
    ylabel('Principal component');
    iD = num2str(i);
    if (assimilatedModel)
        assimilatedExtra = [' (assim., \sigma ' num2str(sigmaModel) ')'];
        assimilatedFile = ['_assimilated_sigma' num2str(sigmaModel)];
        modelName = 'Assim. model';
    else
        assimilatedExtra = '';
        assimilatedFile = '';
        modelName = 'Model';
    end
    legendLabels = {['PC ' iD ' proj. ' lower(modelName)], ['PC ' iD ' HadCRUT data']};
    hold on;
    if (nYearAverage > 0)
        x=1850+nYearAverage:nYearAverage:1999;
    else
        x=1:length(time);
        set(gca,'xTick',1:300:1801)
        set(gca,'xTickLabel',{'1850', '1875', '1900', '1925', '1950', '1975','2000'})
        axis([1 1801 -inf inf]);
    end
    plot(x,projectedPCs(:,i));
    plot(x,dataPCs(:,i));
    ax = gca;
    ax.YLim = [-0.4 * (max([projectedPCs(:,i); dataPCs(:,i)]) -  min([projectedPCs(:,i); dataPCs(:,i)])) + min([projectedPCs(:,i); dataPCs(:,i)]) inf];
    if (assimilated && compareWithOrig)
        plot(x,projectedPCsOrig(:,i),'-.','Color',colorOrig);
    end
    %plot(x,modelPCs(:,i));
    legend(legendLabels,'Location','south','Orientation','horizontal');
    legend('boxoff');
    corrPCmat = corrcoef(projectedPCs(:,i),dataPCs(:,i));
    corrPC = corrPCmat(1,2);
    corrCoefficientsModelOnData(q) = corrPC;
    rmsErrorsModelOnData(q) = rms(projectedPCs(:,i) - dataPCs(:,i));

    if (nYearAverage > 0)
        titlestr = sprintf('PC %d of %d year average, hemisph.: %s%s\nCorrelation projected/data: %1.3f\n%s projected on HadCRUT data' ,i, nYearAverage, hemisphere{1}, assimilatedExtra, corrPC, modelName);
    else
        titlestr = sprintf('PC %d (no average), hemisph: %s%s\nCorrelation projected/data: %1.3f\n%s projected on HadCRUT data' ,i, hemisphere{1}, assimilatedExtra, corrPC, modelName);
    end
    %title(titlestr);
    set(gcf, 'Color', 'w');
    drawnow;
    if (nYearAverage > 0)
        imgname = sprintf('PC%d_modelondata%s_%dyearAverage_%s',i,assimilatedFile,nYearAverage,hemisphere{1});
    else
        imgname = sprintf('PC%d_modelondata%s_%s',i,assimilatedFile,hemisphere{1});
    end
    if (savePNG)
        drawnow;
        export_fig([imgname '.png'], '-m2.5');
    end
    if (savePDF)
        drawnow;
        print([imgname '.pdf'], '-dpdf');
    end
end

%load coast;
%figure(5); clf; geoshow(reshape(modelEOFs(:,1), [64 32])', [1/5.6250 90 0], 'DisplayType', 'texturemap');
%geoshow(lat, long);
%caxis([-0.1 0.1]);




logMsg('Correlation coefficients and RMS errors:', clock);
corrCoefficientsModelOnData
rmsErrorsModelOnData
corrCoefficientsDataOnModel
rmsErrorsDataOnModel


%% Finalize

logMsg('Done.', clock);

end